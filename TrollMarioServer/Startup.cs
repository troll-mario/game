using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TrollMarioServer.Hubs;
using TrollMarioServer.Interfaces;
using TrollMarioServer.Services;

namespace TrollMarioServer
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();

            services.AddSingleton<IState, StateService>();

            services.AddScoped<GameLoop>();

            services.AddTransient<ActionValidationService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(builder =>
            {
                builder.MapHub<GameHub>("/gameHub");
            });
        }
    }
}
