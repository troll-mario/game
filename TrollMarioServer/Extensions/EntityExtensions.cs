﻿using System;
using System.Text;
using TrollMarioServer.Dto;
using System.Windows;
using SFML.Graphics;

namespace TrollMarioServer.Extensions
{
    public static class EntityExtensions
    {
        public static bool Intersects(this GameEntity entity, GameEntity other)
        {
            var entityBoundaries = new FloatRect(entity.PositionX, entity.PositionY, entity.Width, entity.Height);
            var otherBoundaries = new FloatRect(other.PositionX, other.PositionY, other.Width, other.Height);

            return entityBoundaries.Intersects(otherBoundaries);
        }
    }
}