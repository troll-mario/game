﻿using TrollMarioDomain.Dto;
using TrollMarioDomain.Enums;
using TrollMarioServer.Dto;
using TrollMarioServer.Extensions;
using TrollMarioServer.Interfaces;
using ObstacleEntity = TrollMarioServer.Dto.ObstacleEntity;

namespace TrollMarioServer.Services
{
    public class ActionValidationService
    {

        private readonly IState _stateService;

        public ActionValidationService(IState stateService)
        {
            _stateService = stateService;
        }

        public int ValidateHealthUpdate(HealthUpdate update)
        {
            var damageDealer = _stateService.GetEntity(update.DealerId);
            var receiver = _stateService.GetEntity(update.ReceiverId);

            // terrible logic for the long run, but gets the job done for current implementation with no server side map

            if (damageDealer is null || receiver is null)
            {
                return 0;
            }

            if (receiver.Id == damageDealer.Id)
            {
                return update.Amount;
            }

            if (!(receiver is PlayerEntity player))
            {
                return 0;
            }

            if (damageDealer is ObstacleEntity obstacle && obstacle.Type == ObstacleType.Spikes)
            {
                return update.Amount;
            }

            return 0;
        }
    }
}