﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrollMarioDomain.Dto;
using TrollMarioDomain.Enums;
using TrollMarioServer.Dto;
using TrollMarioServer.Interfaces;
using ObstacleEntity = TrollMarioServer.Dto.ObstacleEntity;

namespace TrollMarioServer.Services
{
    public partial class StateService
    {
        private class GameState : IState
        {
            private const int MaxPlayers = 4;

            private int _idCounter;

            private readonly IDictionary<string, PlayerEntity> _players;
            private readonly IDictionary<int, NpcEntity> _npcs;
            private readonly IDictionary<int, ObstacleEntity> _obstacles;

            public GameState()
            {
                _idCounter = 0;
                _players = new Dictionary<string, PlayerEntity>();
                _npcs = new Dictionary<int, NpcEntity>();
                _obstacles = new Dictionary<int, ObstacleEntity>();
            }

            public void AddPlayer(PlayerEntity player)
            {
                player.Id = _idCounter++;
                player.Health = 1;

                if (_players.Count < MaxPlayers)
                {
                    _players.Add(player.ConnectionId, player);
                }
                else
                {
                    throw new InvalidOperationException("Maximal number of players already connected.");
                }
            }

            public void AddNpcs(IEnumerable<NpcEntity> npcs)
            {
                foreach (var npc in npcs)
                {
                    npc.Id = _idCounter++;

                    _npcs.Add(npc.Id, npc);
                }
            }

            public void AddObstacles(IEnumerable<ObstacleEntity> obstacles)
            {
                foreach (var obstacle in obstacles)
                {
                    _obstacles.Add(obstacle.Id, obstacle);
                }
            }

            public GameEntity GetEntity(int id)
            {
                _obstacles.TryGetValue(id, out var obstacle);
                if (obstacle is object)
                {
                    return obstacle;
                }

                _npcs.TryGetValue(id, out var npc);
                if (npc is object)
                {
                    return npc;
                }

                foreach (var (_, player) in _players)
                {
                    if (player.Id == id)
                    {
                        return player;
                    }
                }

                Console.WriteLine("Entity with id {id} does not exist");
                return null;
            }

            public PlayerEntity GetPlayer(string connectionId)
            {
                return _players[connectionId];
            }

            public IEnumerable<(string, EntityState)> GetPlayerStates()
            {
                return _players.Values.Select(p =>
                (
                    p.ConnectionId,
                    new EntityState
                    {
                        Id = p.Id,
                        PositionX = p.PositionX,
                        PositionY = p.PositionY,
                        Width = p.Width,
                        Height = p.Height,
                        VelocityX = p.VelocityX,
                        VelocityY = p.VelocityY
                    }
                ));
            }

            public IEnumerable<NpcState> GetNpcStates()
            {
                return _npcs.Values.Select(n => new NpcState
                {
                    Id = n.Id,
                    PositionX = n.PositionX,
                    PositionY = n.PositionY,
                    VelocityX = n.VelocityX,
                    VelocityY = n.VelocityY,
                    Type = n.Type
                });
            }

            public void RemovePlayer(string connectionId)
            {
                if (_players.ContainsKey(connectionId))
                {
                    _players.Remove(connectionId);
                }
                else
                {
                    throw new InvalidOperationException($"Player {connectionId} was not found.");
                }
            }

            public void RemoveNpcs()
            {
                _npcs.Clear();
            }

            public void RemoveNpcs(IEnumerable<int> ids)
            {
                var exceptions = new List<Exception>();

                foreach (var id in ids)
                {
                    if (_npcs.ContainsKey(id))
                    {
                        _npcs.Remove(id);
                    }
                    else
                    {
                        exceptions.Add(new InvalidOperationException($"NPC {id} was not found."));
                    }
                }

                if (exceptions.Count > 0)
                {
                    throw new AggregateException(exceptions);
                }
            }

            public void SetNpcAction(int id, Action<NpcEntity> action)
            {
                if (_npcs.TryGetValue(id, out var npc))
                {
                    npc.Action = action;
                }
                else
                {
                    throw new InvalidOperationException($"NPC {id} was not found.");
                }
            }

            public void Shoot(EntityState shooter, Direction direction)
            {
                if (!shooter.HasPosition)
                {
                    throw new InvalidOperationException("Shooter with no position can not shoot.");
                }

                AddNpcs(new[]
                {
                    new ProjectileEntity
                    {
                        Action = npc =>
                        {
                            var projectile = (ProjectileEntity)npc;

                            if (projectile.Distance >= projectile.Range)
                            {
                                RemoveNpcs(new[] { projectile.Id });
                            }
                            else
                            {
                                switch (projectile.Direction)
                                {
                                    case Direction.Down:
                                        projectile.Distance += projectile.Speed;
                                        projectile.PositionY += projectile.Speed;
                                        projectile.VelocityX = 0;
                                        projectile.VelocityY = projectile.Speed;

                                        break;
                                    case Direction.Left:
                                        projectile.Distance += projectile.Speed;
                                        projectile.PositionX -= projectile.Speed;
                                        projectile.VelocityX = -projectile.Speed;
                                        projectile.VelocityY = 0;

                                        break;
                                    case Direction.Right:
                                        projectile.Distance += projectile.Speed;
                                        projectile.PositionX += projectile.Speed;
                                        projectile.VelocityX = projectile.Speed;
                                        projectile.VelocityY = 0;

                                        break;
                                    case Direction.Up:
                                        projectile.Distance += projectile.Speed;
                                        projectile.PositionY -= projectile.Speed;
                                        projectile.VelocityX = 0;
                                        projectile.VelocityY = -projectile.Speed;

                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException(nameof(projectile.Direction), projectile.Direction, null);
                                }
                            }
                        },
                        Direction = direction,
                        Distance = 0,
                        PositionX = shooter.PositionX.Value,
                        PositionY = shooter.PositionY.Value,
                        Range = 10,
                        Speed = 1,
                        Type = NpcType.Projectile
                    }
                });
            }

            public void UpdateNpcs()
            {
                foreach (var npc in _npcs.Values)
                {
                    npc.Update();
                }
            }

            public void UpdatePlayer(string connectionId, PlayerEntity state)
            {
                var player = _players[connectionId];

                player.PositionX = state.PositionX;
                player.PositionY = state.PositionY;
                player.VelocityX = state.VelocityX;
                player.VelocityY = state.VelocityY;
                player.Health = state.Health;
            }
        }
    }
}
