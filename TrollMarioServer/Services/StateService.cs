﻿using System;
using System.Collections.Generic;
using System.Threading;
using TrollMarioDomain.Dto;
using TrollMarioDomain.Enums;
using TrollMarioServer.Dto;
using TrollMarioServer.Interfaces;
using ObstacleEntity = TrollMarioServer.Dto.ObstacleEntity;

namespace TrollMarioServer.Services
{
    public partial class StateService : IState
    {
        private readonly ReaderWriterLockSlim _lock;
        private readonly IState _state;

        public StateService()
        {
            _state = new GameState();
            _lock = new ReaderWriterLockSlim();
        }

        public void AddPlayer(PlayerEntity player)
        {
            Write(() => _state.AddPlayer(player));
        }

        public void AddNpcs(IEnumerable<NpcEntity> npcs)
        {
            Write(() => _state.AddNpcs(npcs));
        }

        public void AddObstacles(IEnumerable<ObstacleEntity> obstacles)
        {
            Write(() => _state.AddObstacles(obstacles));
        }

        public PlayerEntity GetPlayer(string connectionId)
        {
            return Read(() => _state.GetPlayer(connectionId));
        }

        public GameEntity GetEntity(int id)
        {
            return Read(() => _state.GetEntity(id));
        }

        public IEnumerable<(string, EntityState)> GetPlayerStates()
        {
            return Read(() => _state.GetPlayerStates());
        }

        public IEnumerable<NpcState> GetNpcStates()
        {
            return Read(() => _state.GetNpcStates());
        }

        public void RemovePlayer(string connectionId)
        {
            Write(() => _state.RemovePlayer(connectionId));
        }

        public void RemoveNpcs()
        {
            Write(_state.RemoveNpcs);
        }

        public void RemoveNpcs(IEnumerable<int> ids)
        {
            Write(() => _state.RemoveNpcs(ids));
        }

        public void SetNpcAction(int id, Action<NpcEntity> action)
        {
            Write(() => _state.SetNpcAction(id, action));
        }

        public void Shoot(EntityState shooter, Direction direction)
        {
            Write(() => _state.Shoot(shooter, direction));
        }

        public void UpdatePlayer(string connectionId, PlayerEntity state)
        {
            Write(() => _state.UpdatePlayer(connectionId, state));
        }

        public void UpdateNpcs()
        {
            Write(_state.UpdateNpcs);
        }

        private T Read<T>(Func<T> func)
        {
            try
            {
                _lock.EnterReadLock();

                return func.Invoke();
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        private void Write(Action action)
        {
            try
            {
                _lock.EnterWriteLock();

                action.Invoke();
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }
    }
}
