﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading;
using System.Threading.Tasks;
using TrollMarioDomain.Enums;
using TrollMarioServer.Dto;
using TrollMarioServer.Hubs;
using TrollMarioServer.Interfaces;

namespace TrollMarioServer.Services
{
    public class GameLoop
    {
        private bool _stop;

        private readonly object _lock;
        private readonly IHubContext<GameHub> _gameHubContext;
        private readonly IState _stateService;

        public GameLoop
            (
                IHubContext<GameHub> gameHubContext,
                IState stateService
            )
        {
            _stop = true;
            _lock = new object();
            _gameHubContext = gameHubContext;
            _stateService = stateService;
        }

        public void Init()
        {
            lock (_lock)
            {
                if (_stop)
                {
                    _stop = false;

                    _stateService.AddNpcs(new[]
                    {
                        new NpcEntity
                        {
                            Action = npc =>
                            {
                                npc.PositionX++;
                            },
                            PositionX = 0,
                            PositionY = 100,
                            Type = NpcType.Goomba
                        }
                    });

                    // while no map read
                    _stateService.AddObstacles(new []
                    {
                        new ObstacleEntity
                        {
                            PositionX = 750,
                            PositionY = 600,
                            Id = 1378,
                            Height = 20,
                            Width = 50,
                            Type = ObstacleType.Spikes
                        } 
                    });
                }
                else
                {
                    throw new InvalidOperationException("Game loop is running.");
                }
            }
        }

        public void Run()
        {
            Task.Run(async () =>
            {
                while (true)
                {
                    lock (_lock)
                    {
                        if (_stop) break;
                    }

                    await Tick();

                    Thread.Sleep(10);
                }
            });
        }

        public void Stop()
        {
            lock (_lock)
            {
                _stop = true;

                _stateService.RemoveNpcs();
            }
        }

        private async Task Tick()
        {
            _stateService.UpdateNpcs();

            await _gameHubContext.Clients.All.SendAsync("UpdateNpcs", _stateService.GetNpcStates());
        }
    }
}
