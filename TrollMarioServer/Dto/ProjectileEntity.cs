﻿using TrollMarioDomain.Enums;

namespace TrollMarioServer.Dto
{
    public class ProjectileEntity : NpcEntity
    {
        public Direction Direction { get; set; }

        public float Distance { get; set; }

        public float Range { get; set; }

        public float Speed { get; set; }
    }
}
