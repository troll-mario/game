﻿namespace TrollMarioServer.Dto
{
    public class PlayerEntity : GameEntity
    {
        public string ConnectionId { get; set; }
        public int Health { get; set; }
    }
}
