﻿using System;
using TrollMarioDomain.Enums;

namespace TrollMarioServer.Dto
{
    public class NpcEntity : GameEntity
    {
        public Action<NpcEntity> Action { get; set; }

        public NpcType Type { get; set; }

        public void Update() {
            Action?.Invoke(this);
        }
    }
}
