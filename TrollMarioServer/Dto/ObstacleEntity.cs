﻿using TrollMarioDomain.Enums;

namespace TrollMarioServer.Dto
{
    public class ObstacleEntity : GameEntity
    {
        public ObstacleType Type;
    }
}