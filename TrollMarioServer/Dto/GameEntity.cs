﻿using Microsoft.Net.Http.Headers;

namespace TrollMarioServer.Dto
{
    public abstract class GameEntity
    {
        public int Id { get; set; }

        public float PositionX { get; set; }

        public float PositionY { get; set; }

        public float Width { get; set; }

        public float Height { get; set; }

        public float VelocityX { get; set; }

        public float VelocityY { get; set; }
    }
}
