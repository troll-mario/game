﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Linq;
using System.Threading.Tasks;
using TrollMarioDomain.Dto;
using TrollMarioDomain.Enums;
using TrollMarioServer.Dto;
using TrollMarioServer.Interfaces;
using TrollMarioServer.Services;

namespace TrollMarioServer.Hubs
{
    public class GameHub : Hub
    {
        private static int _count = 0;
        private static readonly object _lock = new object();

        private readonly GameLoop _gameLoop;
        private readonly IState _stateService;
        private readonly ActionValidationService _actionValidationService;

        public GameHub
            (
                IState stateService,
                GameLoop gameLoop, 
                ActionValidationService actionValidationService
            )
        {
            _gameLoop = gameLoop;
            _actionValidationService = actionValidationService;
            _stateService = stateService;
        }

        public override async Task OnConnectedAsync()
        {
            try
            {
                lock (_lock)
                {
                    if (++_count == 1)
                    {
                        _gameLoop.Init();
                    }
                }

                var player = new PlayerEntity
                {
                    ConnectionId = Context.ConnectionId
                };

                _stateService.AddPlayer(player);

                await Clients.Caller.SendAsync("SetId", player.Id);

                var opponents = _stateService
                    .GetPlayerStates()
                    .Where(ps => ps.Item1 != Context.ConnectionId)
                    .Select(ps => ps.Item2);

                foreach (var opponent in opponents)
                {
                    await Clients.Caller.SendAsync("InitializeOpponent", opponent);
                }

                await Clients.Others.SendAsync("InitializeOpponent", new EntityState
                {
                    Id = player.Id
                });

                await Clients.Caller.SendAsync("AddNpcs", _stateService.GetNpcStates());

                lock (_lock)
                {
                    if (_count == 1)
                    {
                        _gameLoop.Run();
                    }
                }
            }
            catch (Exception e)
            {
                await Clients.Caller.SendAsync("DisconnectClient", e.Message);
            }
            finally
            {
                await base.OnConnectedAsync();
            }
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            try
            {
                await Clients.Others.SendAsync("RemoveEntity", _stateService.GetPlayer(Context.ConnectionId).Id);

                _stateService.RemovePlayer(Context.ConnectionId);

                lock (_lock)
                {
                    if (--_count == 0)
                    {
                        _gameLoop.Stop();
                    }
                }
            }
            catch (Exception e)
            {
                await base.OnDisconnectedAsync(e);

                return;
            }

            await base.OnDisconnectedAsync(exception);
        }

        public void Shoot(EntityState shooter, Direction direction)
        {
            _stateService.Shoot(shooter, direction);
        }

        public async Task UpdateHealth(HealthUpdate update)
        {
            var actualDifference = _actionValidationService.ValidateHealthUpdate(update);
            if (actualDifference != 0)
            {
                var entity = _stateService.GetEntity(update.ReceiverId);
                if (entity is PlayerEntity player)
                {
                    player.Health += actualDifference;
                    _stateService.UpdatePlayer(player.ConnectionId, player);
                }
            }

            await Clients.Caller.SendAsync($"{nameof(UpdateHealth)}_{update.ReceiverId}", actualDifference);
        }

        public async Task UpdatePosition(EntityState state)
        {
            var mappedEntity = new PlayerEntity
            {
                ConnectionId = Context.ConnectionId,
                Health = state.Health.Value,
                Height = state.Height,
                Id = state.Id,
                PositionX = state.PositionX.Value,
                PositionY = state.PositionY.Value,
                VelocityX = state.VelocityX.Value,
                VelocityY = state.VelocityY.Value
            };
            _stateService.UpdatePlayer(Context.ConnectionId, mappedEntity);

            await Clients.Others.SendAsync("UpdatePosition", state);
        }

        public async Task ReceiveChatMessage(string message)
        {
            await Clients.All.SendAsync("ReceiveChatMessage", "Player " + _stateService.GetPlayer(Context.ConnectionId).Id + ": " + message);
        }
    }
}
