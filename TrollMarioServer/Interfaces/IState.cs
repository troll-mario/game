﻿using System;
using System.Collections.Generic;
using TrollMarioDomain.Dto;
using TrollMarioDomain.Enums;
using TrollMarioServer.Dto;
using ObstacleEntity = TrollMarioServer.Dto.ObstacleEntity;

namespace TrollMarioServer.Interfaces
{
    public interface IState
    {
        void AddPlayer(PlayerEntity player);

        void AddNpcs(IEnumerable<NpcEntity> npcs);

        void AddObstacles(IEnumerable<ObstacleEntity> obstacles);

        PlayerEntity GetPlayer(string connectionId);

        void RemovePlayer(string connectionId);

        IEnumerable<(string, EntityState)> GetPlayerStates();

        IEnumerable<NpcState> GetNpcStates();

        void RemoveNpcs();

        void RemoveNpcs(IEnumerable<int> ids);

        void SetNpcAction(int id, Action<NpcEntity> action);

        void Shoot(EntityState shooter, Direction direction);

        void UpdateNpcs();

        void UpdatePlayer(string connectionId, PlayerEntity state);

        GameEntity GetEntity(int id);
    }
}
