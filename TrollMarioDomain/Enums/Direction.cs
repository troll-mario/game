﻿namespace TrollMarioDomain.Enums
{
    public enum Direction
    {
        Up,
        Right,
        Down,
        Left
    }
}
