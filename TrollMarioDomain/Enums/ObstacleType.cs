﻿namespace TrollMarioDomain.Enums
{
    public enum ObstacleType
    {
        Spikes,
        Vines
    }
}