﻿namespace TrollMarioDomain.Enums
{
    public enum NpcType
    {
        Goomba,
        Koopa,
        Projectile
    }
}
