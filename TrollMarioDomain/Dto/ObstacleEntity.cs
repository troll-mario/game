﻿using TrollMarioDomain.Enums;

namespace TrollMarioDomain.Dto
{
    public class ObstacleEntity : EntityState
    {
        public ObstacleType Type;
    }
}