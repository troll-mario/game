﻿namespace TrollMarioDomain.Dto
{
    public class EntityState
    {
        public int Id { get; set; }
        public float? PositionX { get; set; }
        public float? PositionY { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public float? VelocityX { get; set; }
        public float? VelocityY { get; set; }
        public int? Health { get; set; }

        public bool HasHealth => Health.HasValue;
        public bool HasPosition => PositionX.HasValue && PositionY.HasValue;
        public bool HasVelocity => VelocityX.HasValue && VelocityY.HasValue;

        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"Health: {Health}\n" +
                $"PositionX: {PositionX}\n" +
                $"PositionY: {PositionY}\n" +
                $"VelocityX: {VelocityX}\n" +
                $"VelocityY: {VelocityY}\n";
        }
    }
}
