﻿using System.Collections.Generic;

namespace TrollMarioDomain.Dto
{
    public class PlayerState : EntityState
    {
        public ICollection<string> Powerups { get; set; }
    }
}