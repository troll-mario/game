﻿namespace TrollMarioDomain.Dto
{
    public class HealthUpdate
    {
        public int DealerId { get; set; }
        public int Amount { get; set; }
        public int ReceiverId { get; set; }
    }
}