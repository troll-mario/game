﻿using TrollMarioDomain.Enums;

namespace TrollMarioDomain.Dto
{
    public class NpcState : EntityState
    {
        public NpcType Type { get; set; }
    }
}
