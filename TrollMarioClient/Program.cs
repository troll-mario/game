﻿using Microsoft.Extensions.Configuration;
using TrollMarioClient.Builders.Player;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Obstacles;
using TrollMarioClient.Factories;
using TrollMarioClient.Providers;
using TrollMarioClient.Services;
using Unity;
using Unity.Lifetime;

namespace TrollMarioClient
{
    internal class Program
    {
        private static IUnityContainer _container;

        private static ITypeLifetimeManager DefaultLifeTimeManager => new PerResolveLifetimeManager();

        static void Main()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            GameWindowProvider.Height = configuration.GetValue<uint>("Screen:Height");
            GameWindowProvider.Width = configuration.GetValue<uint>("Screen:Width");

            InitContainer(configuration);

            _container
                .Resolve<Game>()
                .Run();
        }

        private static void InitContainer(IConfiguration configuration)
        {
            _container = new UnityContainer();

            _container.RegisterInstance(configuration);

            _container.RegisterSingleton<Game>();
            //_container.RegisterSingleton<StateServiceSingleton>(); // made as proper sigleton
            _container.RegisterSingleton<IdProvider>();
            _container.RegisterSingleton<MainLoopService>();
            _container.RegisterSingleton<CollisionService>();
            _container.RegisterSingleton<TextureFactory>();
            _container.RegisterSingleton<ServerService>();

            _container.RegisterType<ConsoleReaderService>(DefaultLifeTimeManager);
            _container.RegisterType<DrawingService>(DefaultLifeTimeManager);
            _container.RegisterType<PlayerFactory>(DefaultLifeTimeManager);
            _container.RegisterType<TileFactory>(DefaultLifeTimeManager);
            _container.RegisterType<PlayerSpawnService>(DefaultLifeTimeManager);
            _container.RegisterType<LevelSpawnService>(DefaultLifeTimeManager);
            _container.RegisterType<LocalControlPlayerBuilder>(DefaultLifeTimeManager);
            _container.RegisterType<RemoteControlPlayerBuilder>(DefaultLifeTimeManager);
            _container.RegisterType<NpcFactory>(DefaultLifeTimeManager);
            _container.RegisterType<PowerupFactory>(DefaultLifeTimeManager);
            _container.RegisterType<InvisibleObstacleFactory>(DefaultLifeTimeManager);
            _container.RegisterType<VisibleObstacleFactory>(DefaultLifeTimeManager);
            _container.RegisterType<FactoryFacade>(DefaultLifeTimeManager);
            _container.RegisterType<ObstacleTileFactoryAdapter>(DefaultLifeTimeManager);
            _container.RegisterType<ObstacleDirector>(DefaultLifeTimeManager);


            UnityContainerProvider.Container = _container;
        }
    }
}
