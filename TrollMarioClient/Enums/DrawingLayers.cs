﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrollMarioClient.Enums
{
    public enum DrawingLayers
    {
        Background,
        Static,
        Obstacle,
        Npc,
        Player,
        MiniMap,
        Default
    }
}
