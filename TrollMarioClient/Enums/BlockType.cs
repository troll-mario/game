﻿namespace TrollMarioClient.Enums
{
    public enum BlockType
    {
        Background,
        Ground,
        Bricks,
        ShooterPowerup,
        ShieldPowerup,
        StarPowerup,
        VisibleVines = 100,
        VisibleSpikes,
        VisibleWoodenSpikes,
        InvisibleVines,
        InvisibleSpikes,
        InvisibleWoodenSpikes,
        GhostBlock
    }
}