﻿using System;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Enums.Extensions
{
    public static class DirectionExtensions
    {
        public static Direction Invert(this Direction direction)
        {
            return direction switch
            {
                Direction.Up => Direction.Down,
                Direction.Right => Direction.Left,
                Direction.Down => Direction.Up,
                Direction.Left => Direction.Right,
                _ => throw new ArgumentOutOfRangeException(nameof(direction), direction, null)
            };
        }

        public static Direction? Invert(this Direction? direction)
        {
            if (direction.HasValue)
            {
                return direction.Value.Invert();
            }

            return null;
        }
    }
}