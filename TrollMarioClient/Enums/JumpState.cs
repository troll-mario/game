﻿namespace TrollMarioClient.Enums
{
    public enum JumpState
    {
        NotStarted,
        FirstSecondDisabled,
        FirstSecondEnabled,
        Second
    }
}
