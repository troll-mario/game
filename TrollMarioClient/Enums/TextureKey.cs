﻿namespace TrollMarioClient.Enums
{
    public enum TextureKey
    {
        Mario,
        Spikes,
        WoodenSpikes,
        BrokenWoodenSpikes,
        Vines,
        QuestionBlock,
        Bricks,
        Ground,
        Projectile
    }
}