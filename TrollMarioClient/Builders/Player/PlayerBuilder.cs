﻿using SFML.Graphics;
using SFML.System;
using TrollMarioClient.Entities.Components.Checkpoints;
using TrollMarioClient.Entities.Components.Collider;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Entities.Components.Health;
using TrollMarioClient.Enums;
using TrollMarioClient.Factories;
using TrollMarioClient.Services;
using Unity;
using Unity.Resolution;

namespace TrollMarioClient.Builders.Player
{
    public abstract class PlayerBuilder
    {
        protected Entities.Player Player;

        private readonly CollisionService _collisionService;
        private readonly TextureFactory _textureFactory;
        private readonly UnityContainer _unityContainer;

        protected PlayerBuilder(CollisionService collisionService, 
            TextureFactory textureFactory, 
            UnityContainer unityContainer)
        {
            _collisionService = collisionService;
            _textureFactory = textureFactory;
            _unityContainer = unityContainer;
        }

        public abstract PlayerBuilder AddControl();
        public abstract PlayerBuilder AddPowerupSupport();

        public PlayerBuilder AddDrawing(Color color)
        {
            var drawingComponent = new DrawComponent
            {
                Color = color,
                TextureContainer = _textureFactory.GetContainer(TextureKey.Mario)
            };

            drawingComponent.SetDrawingLayer(DrawingLayers.Player);
            Player.AddComponent(drawingComponent);

            return this;
        }

        public PlayerBuilder Start(Vector2f position)
        {
            Player = _unityContainer
                .Resolve<Entities.Player>(new ParameterOverride(nameof(position), position));

            return this;
        }

        public PlayerBuilder AddColisions()
        {
            Player.AddComponent(new StopColliderComponent(_collisionService));

            return this;
        }

        public PlayerBuilder AddHealth()
        {
            Player.AddComponent(new HealthComponent());

            return this;
        }

        public PlayerBuilder AddCheckpoints()
        {
            Player.AddComponent(new CheckpointComponent());

            return this;
        }

        public Entities.Player Build()
        {
            return Player;
        }
    }
}
