﻿using TrollMarioClient.Entities.Components.Control;
using TrollMarioClient.Entities.Components.Movement;
using TrollMarioClient.Entities.Components.Powerups.Managers;
using TrollMarioClient.Factories;
using TrollMarioClient.Services;
using Unity;

namespace TrollMarioClient.Builders.Player
{
    public class RemoteControlPlayerBuilder : PlayerBuilder
    {
        public RemoteControlPlayerBuilder(CollisionService collisionService, TextureFactory textureFactory,
            UnityContainer unityContainer) : base(collisionService, textureFactory, unityContainer)
        {
        }

        public override PlayerBuilder AddControl()
        {
            var movement = new RemoteMovementComponent();
            var control = new RemoteControlComponent();

            Player.AddComponent(control);
            Player.AddComponent(movement);

            return this;
        }

        public override PlayerBuilder AddPowerupSupport()
        {
            Player.AddComponent(new RemotePlayerPowerupManager());

            return this;
        }
    }
}