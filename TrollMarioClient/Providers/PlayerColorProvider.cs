﻿using SFML.Graphics;

namespace TrollMarioClient.Providers
{
    public static class PlayerColorProvider
    {
        private static readonly Color[] Colors = { Color.Red, Color.Green, Color.Blue, Color.Magenta, Color.Cyan, Color.Yellow };

        public static Color Get(int playerId)
        {
            return Colors[playerId % Colors.Length];
        }
    }
}
