﻿namespace TrollMarioClient.Providers
{
    public class IdProvider
    {
        private int _currentId = 1001; // PROTOTYPING

        public int Next()
        {
            return ++_currentId;
        }
    }
}
