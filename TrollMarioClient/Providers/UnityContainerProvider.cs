﻿using System;
using Unity;

namespace TrollMarioClient.Providers
{
    public static class UnityContainerProvider
    {
        private static IUnityContainer _container;

        public static IUnityContainer Container
        {
            get
            {
                if (_container is null)
                {
                    throw new InvalidOperationException();
                }

                return _container;
            }
            set
            {
                if (_container is object || value is null)
                {
                    throw new InvalidOperationException();
                }

                _container = value;
            }
        }
    }
}