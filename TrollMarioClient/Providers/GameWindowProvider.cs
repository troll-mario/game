﻿using SFML.Graphics;
using System;

namespace TrollMarioClient.Providers
{
    public class GameWindowProvider
    {
        public static readonly Color Background = new Color(3, 182, 252);
        private static uint _height = 720;
        private static uint _width = 1280;
        private static RenderWindow _window;
        private static readonly object _windowLock = new object();

        public static uint Height
        {
            get => _height;
            set
            {
                if (_window is object)
                {
                    throw new InvalidOperationException("Game window already initialized");
                }

                _height = value;
            }
        }

        public static uint Width
        {
            get => _width;
            set
            {
                if (_window is object)
                {
                    throw new InvalidOperationException("Game window already initialized");
                }

                _width = value;
            }
        }

        public static RenderWindow Window
        {
            get
            {
                lock (_windowLock)
                {
                    if (_window is null)
                    {
                        _window = new RenderWindow(new SFML.Window.VideoMode(_width, _height), "GameProject");

                        _window.SetVerticalSyncEnabled(true);
                    }
                }
               
                return _window;
            }
        }
    }
}
