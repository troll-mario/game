﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Components.Control;
using TrollMarioClient.Utilities.Memento;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioDomain.Dto;

namespace TrollMarioClient.Services
{
    public class StateServiceSingleton
    {
        private readonly IDictionary<int, GameEntity> _entities;
        private readonly ReaderWriterLockSlim _lock;

        private Player _player;

        private StateServiceSingleton()
        {
            _entities = new Dictionary<int, GameEntity>();
            _lock = new ReaderWriterLockSlim();
        }

        public static StateServiceSingleton GetInstance()
        {
            return StateServiceHolder.StateService;
        }

        public void SetMainPlayer(Player player)
        {
            if (_player is null)
            {
                _player = player;
            }
            else
            {
                throw new InvalidOperationException("Player already exists");
            }
        }

        public void WriteWithLock(Action<IDictionary<int, GameEntity>> writeAction)
        {
            try
            {
                _lock.EnterWriteLock();

                writeAction(_entities);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }
        
        public IEnumerable<Drawable> GetDrawables()
        {
            try
            {
                _lock.EnterReadLock();

                return _entities.Values
                    .Select(e => e.DrawComponent.Drawable)
                    .ToList();
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        public IEnumerable<DrawComponent> GetDrawComponents()
        {
            try
            {
                _lock.EnterReadLock();

                return _entities.Values
                    .Select(e => (DrawComponent) e.DrawComponent)
                    .ToList()
                    .OrderBy(x => x.GetDrawingLayer());
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        public EntityState GetMainPlayerState()
        {
            try
            {
                _lock.EnterReadLock();

                return _player.GetState();
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        public void RemoveEntity(int id)
        {
            try
            {
                _lock.EnterWriteLock();

                _entities.Remove(id);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void SetMainPlayerId(int id)
        {
            WriteWithLock(entities =>
            {
                entities.Remove(_player.Id);

                _player.Id = id;

                entities.Add(_player.Id, _player);
            });
        }

        public void Update()
        {
            WriteWithLock(entities =>
            {
                foreach (var entity in entities.Where(e => e.Value.Destroy))
                {
                    entities.Remove(entity.Key);
                }

                foreach (var entity in entities.Values)
                {
                    entity.Update();
                }
            });
        }

        public void UpdateState(EntityState state)
        {
            WriteWithLock(entities =>
            {
                if (entities.TryGetValue(state.Id, out var entity))
                {
                    (entity.ControlComponent as RemoteControlComponent)?.UpdateState(state);
                    if (state.HasHealth)
                    {
                        entity.HealthComponent.Health = state.Health.Value;
                    }
                }
            });
        }

        private static class StateServiceHolder
        {
            public static StateServiceSingleton StateService = new StateServiceSingleton();
        }

        public PositionMemento SavePositionToMemento()
        {
            return new PositionMemento(_player.Events.InvokeCheckpoint, _player.GetPosition());
        }
    }
}
