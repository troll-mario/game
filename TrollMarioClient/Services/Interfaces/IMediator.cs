﻿using System;
using TrollMarioClient.Entities;

namespace TrollMarioClient.Services.Interfaces
{
    public interface IMediator
    {
        public void Notify(GameEntity sender, int receiverId, EventHandler invokableAction);
    }
}