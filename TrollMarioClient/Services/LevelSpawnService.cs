﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Interfaces;
using TrollMarioClient.Entities.Obstacles;
using TrollMarioClient.Enums;
using TrollMarioClient.Factories;
using TrollMarioClient.Providers;

namespace TrollMarioClient.Services
{
    public class LevelSpawnService
    {
        private readonly StateServiceSingleton _stateService;
        private readonly IdProvider _idProvider;
        private readonly FactoryFacade _factory;

        public LevelSpawnService
            (
                IdProvider idProvider,
                FactoryFacade factory
            )
        {
            _stateService = StateServiceSingleton.GetInstance();
            _idProvider = idProvider;
            _factory = factory;
        }

        public void SpawnLevel(ObstacleDirector dir)
        {
            _stateService.WriteWithLock(entities =>
            {
                using var r = new StreamReader(@"../../../Map.txt");
                var line = r.ReadLine();

                var values = line.Split('\t');

                var tileWidth = Convert.ToInt32(values[0]);
                var mapWidth = Convert.ToInt32(values[1]);
                var mapHeight = Convert.ToInt32(values[2]);

                for (var i = 0; i < mapWidth; i++)
                {
                    line = r.ReadLine();

                    var blockTypes = line.Split('\t')
                        .Select(v => (BlockType)Convert.ToInt32(v)).ToArray();

                    for (var j = 0; j < mapHeight; j++)
                    {
                        if (Convert.ToInt32(blockTypes[j]) < 100)
                        {
                            var tile = SpawnBlock((uint)(i * tileWidth), (uint)(j * tileWidth), blockTypes[j]);
                            entities.Add(tile.Id, tile);
                            
                        }
                        else if (Convert.ToInt32(blockTypes[j]) >= 100 && Convert.ToInt32(blockTypes[j]) < 200)
                        {
                            var tile = _factory.CreateTile(new Vector2f((uint)(j * tileWidth), (uint)(i * tileWidth)), blockTypes[j]);
                            entities.Add(tile.Id, tile);
                            dir.Add((Obstacle)tile);
                        }
                        else
                        {
                            throw new NotImplementedException();
                        }
                    }
                }

                SpawnWorldBoundaries(entities);
            });
        }

        private GameEntity SpawnBlock(uint y, uint x, BlockType type)
        {
            return _factory.CreateTile(new Vector2f(x, y), type);
        }

        public void SpawnWorldBoundaries(IDictionary<int, GameEntity> entities)
        {
            for (var i = 0; i < GameWindowProvider.Height; i += 50)
            {
                entities.Add(_idProvider.Next(), _factory.CreateTile(new Vector2f(-50, i), BlockType.Bricks));
            }
        }
    }
}
