﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TrollMarioClient.Utilities.Interpreter;
using TrollMarioClient.Utilities.Memento;
using TrollMarioClient.Utilities.Interpreter.ConsoleCommands;
using TrollMarioClient.Utilities;
using TrollMarioClient.Entities.Interfaces;
using TrollMarioClient.Entities.Obstacles;

namespace TrollMarioClient.Services
{
    class ConsoleReaderService
    {
        private ServerService _serverService;
        private ObstacleDirector _obstacleDirector;

        public ConsoleReaderService(ServerService serverService)
        {
            _serverService = serverService;
        }

        public void Start(ObstacleDirector dir)
        {
            _obstacleDirector = dir;
            Task ConsoleReadTask = new Task(() => ReadConsoleLoop());
            ConsoleReadTask.Start();
        }

        public void ReadConsoleLoop()
        {
            List<PositionMemento> playerMementos = new List<PositionMemento>();
            ConcreteContainer<AbstractExpression> expressions = new ConcreteContainer<AbstractExpression>();
            ConcreteIterator<AbstractExpression> enumerator = expressions.GetEnumerator();
            enumerator.SetPriorityType(typeof(ChatExpression));

            expressions.Add(new TeleportExpression(new ConsoleCommandTeleport()));
            expressions.Add(new ChatExpression(new ConsoleCommandChat(_serverService)));
            expressions.Add(new SavePlayerStateExpression(new ConsoleCommandSavePlayerState(playerMementos)));
            expressions.Add(new LoadPlayerStateExpression(new ConsoleCommandLoadPlayerState(playerMementos)));
            expressions.Add(new SetObstacleStateExpression(new ConsoleCommandSetObstacleState(_obstacleDirector)));

            while (true)
            {
                string input = Console.ReadLine();
                InterpreterContext context = new InterpreterContext(input);

                enumerator.Reset();
                while(enumerator.HasNext())
                {
                    var exp = enumerator.Next();
                    exp.Evaluate(context);
                }

            }
        }
    }
}
