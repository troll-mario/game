using SFML.System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TrollMarioClient.Entities.Components.Collider;

namespace TrollMarioClient.Services
{
    public class CollisionService
    {
        private readonly IList<ColliderComponentBase> _colliders;
        private readonly ReaderWriterLockSlim _lock;

        public CollisionService()
        {
            _colliders = new List<ColliderComponentBase>();
            _lock = new ReaderWriterLockSlim();
        }

        public void Register(ColliderComponentBase collider)
        {
            try
            {
                _lock.EnterWriteLock();

                _colliders.Add(collider);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void Unregister(ColliderComponentBase collider)
        {
            try
            {
                _lock.EnterWriteLock();

                _colliders.Remove(collider);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public Vector2f TryMove(ColliderComponentBase collider, Vector2f offset)
        {
            var colliders = new List<ColliderComponentBase>();

            try
            {
                _lock.EnterReadLock();

                colliders = _colliders
                    .OrderByDescending(c => c is SlowDownColliderComponent)
                    .ToList();
            }
            finally
            {
                _lock.ExitReadLock();
            }

            foreach (var otherCollider in colliders)
            {
                if (otherCollider.Entity.Id != collider.Entity.Id)
                {
                    offset = collider.TryCollide(offset, otherCollider);
                }
            }

            return offset;
        }
    }
}
