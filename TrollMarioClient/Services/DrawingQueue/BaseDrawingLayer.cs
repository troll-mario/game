﻿using SFML.Graphics;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Providers;

namespace TrollMarioClient.Services.DrawingQueue
{
    public abstract class BaseDrawingLayer
    {
        protected BaseDrawingLayer NextLayer;
        protected readonly RenderWindow Window;

        public BaseDrawingLayer(BaseDrawingLayer layer)
        {
            this.NextLayer = layer;
            Window = GameWindowProvider.Window;
        }

        public virtual void Draw(DrawComponent entity)
        {
            if (NextLayer != null)
            {
                this.NextLayer.Draw(entity);
            }
        }
    }
}
