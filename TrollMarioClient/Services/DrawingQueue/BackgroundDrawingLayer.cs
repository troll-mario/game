﻿using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Services.DrawingQueue
{
    public class BackgroundDrawingLayer : BaseDrawingLayer
    {
        public BackgroundDrawingLayer(BaseDrawingLayer layer) : base(layer)
        {

        }

        public override void Draw(DrawComponent entity)
        {
            if (entity.GetDrawingLayer().Equals(DrawingLayers.Background))
            {
                this.Window.Draw(entity.Drawable);
            }
            else
            {
                this.NextLayer.Draw(entity);
            }
        }
    }
}
