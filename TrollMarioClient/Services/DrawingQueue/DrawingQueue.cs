﻿using TrollMarioClient.Entities.Components.Drawing;

namespace TrollMarioClient.Services.DrawingQueue
{
    public class DrawingQueue
    {
        private BaseDrawingLayer _queue;

        public DrawingQueue()
        {
            BuildQueue();
        }

        private void BuildQueue()
        {
            this._queue = new BackgroundDrawingLayer(new StaticObjectDrawingLayer(new ObstacleDrawingLayer(new NpcDrawingLayer(new PlayerDrawingLayer(new MiniMapDrawingLayer(new DefaultDrawingLayer(null)))))));
        }

        public void Draw(DrawComponent entity)
        {
            _queue.Draw(entity);
        }
    }
}
