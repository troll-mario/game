﻿using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Services.DrawingQueue
{
    public class PlayerDrawingLayer : BaseDrawingLayer
    {
        public PlayerDrawingLayer(BaseDrawingLayer layer) : base(layer)
        {
        }

        public override void Draw(DrawComponent entity)
        {
            if (entity.GetDrawingLayer().Equals(DrawingLayers.Player))
            {
                this.Window.Draw(entity.Drawable);
            }
            else
            {
                this.NextLayer.Draw(entity);
            }
        }
    }
}
