﻿using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Services.DrawingQueue
{
    public class MiniMapDrawingLayer : BaseDrawingLayer
    {
        public MiniMapDrawingLayer(BaseDrawingLayer layer) : base(layer)
        {
        }

        public override void Draw(DrawComponent entity)
        {
            if (entity.GetDrawingLayer().Equals(DrawingLayers.MiniMap))
            {
                this.Window.Draw(entity.Drawable);
            }
            else
            {
                this.NextLayer.Draw(entity);
            }
        }
    }
}
