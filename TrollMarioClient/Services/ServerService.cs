﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrollMarioClient.Entities.Events;
using TrollMarioDomain.Dto;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Services
{
    public class ServerService : IDisposable
    {
        private readonly HubConnection _hubConnection;

        public ServerService(IConfiguration configuration)
        {
            _hubConnection = new HubConnectionBuilder()
                .WithUrl(configuration.GetValue<string>("Server:Url"))
                .WithAutomaticReconnect()
                .Build();

            _hubConnection.On<string>("DisconnectClient", DisconnectClient);
        }

        public async void Dispose()
        {
            await _hubConnection.DisposeAsync();
        }

        public void On<T>(string name, Action<T> action)
        {
            _hubConnection.Remove(name);

            _hubConnection.On(name, action);
        }

        public void DisposeOnListener(string name)
        {
            _hubConnection.Remove(name);
        }

        public void On<T, K>(string name, Action<T, K> action)
        {
            _hubConnection.Remove(name);

            _hubConnection.On(name, action);
        }

        public async Task Shoot(EntityState shooter, Direction direction)
        {
            await _hubConnection.SendAsync("Shoot", shooter, direction);
        }

        public void StartConnection()
        {
            _hubConnection
                .StartAsync()
                .Wait();

            Console.WriteLine("connected");
        }

        public async Task UpdatePosition(EntityState state)
        {
            await _hubConnection.SendAsync("UpdatePosition", state);
        }

        public async Task UpdateHealth(HealthUpdate healthUpdate)
        {
            await _hubConnection.SendAsync(nameof(UpdateHealth), healthUpdate);
        }

        private async Task DisconnectClient(string reason)
        {
            await _hubConnection.StopAsync();

            Console.WriteLine(reason);
        }

        public async Task ChatMessage(string message)
        {
            await _hubConnection.SendAsync("ReceiveChatMessage", message);
        }
    }
}
