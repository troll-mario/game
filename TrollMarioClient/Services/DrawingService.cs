﻿using System.Collections.Generic;
using System.Linq;
using SFML.Graphics;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Providers;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Services
{
    public class DrawingService
    {
        private readonly StateServiceSingleton _stateService;
        private readonly RenderWindow _window;

        public DrawingService()
        {
            _stateService = StateServiceSingleton.GetInstance();
            _window = GameWindowProvider.Window;
        }

        public void Draw()
        {

            foreach (var drawComponent in _stateService.GetDrawComponents())
            {
                _window.Draw(drawComponent.Drawable);
            }
        }
    }
}
