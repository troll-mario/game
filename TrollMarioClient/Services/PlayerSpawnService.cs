﻿using SFML.System;
using TrollMarioClient.Enums;
using TrollMarioClient.Factories;
using TrollMarioDomain.Dto;

namespace TrollMarioClient.Services
{
    public class PlayerSpawnService
    {
        private readonly StateServiceSingleton _stateService;
        private readonly FactoryFacade _factory;

        public PlayerSpawnService
            (
                FactoryFacade factory
            )
        {
            _stateService = StateServiceSingleton.GetInstance();
            _factory = factory;
        }

        public void SpawnPlayer()
        {
            _stateService.WriteWithLock(entities =>
            {
                var player = _factory.CreatePlayer(ControlType.Local, new Vector2f(0, 400));
                _stateService.SetMainPlayer(player);

                entities.Add(player.Id, player);
            });
        }

        public void SpawnOpponent(EntityState state)
        {
            _stateService.WriteWithLock(entities =>
            {
                var opponent = _factory.CreatePlayer(ControlType.Remote, new Vector2f(0, 0));

                opponent.Id = state.Id;

                if (state.HasPosition)
                {
                    opponent.PositionX = state.PositionX.Value;
                    opponent.PositionY = state.PositionY.Value;
                }

                entities.Add(opponent.Id, opponent);
            });
        }
    }
}
