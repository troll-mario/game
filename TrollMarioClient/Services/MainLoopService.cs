﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TrollMarioClient.Entities.Components.Control;
using TrollMarioClient.Entities.Components.Powerups.Managers;
using TrollMarioClient.Entities.Npc;
using TrollMarioClient.Factories;
using TrollMarioDomain.Dto;

namespace TrollMarioClient.Services
{
    public class MainLoopService
    {
        private readonly ServerService _serverService;
        private readonly PlayerSpawnService _playerSpawnService;
        private readonly StateServiceSingleton _stateService;
        private readonly FactoryFacade _factory;

        public MainLoopService
            (
                ServerService serverService,
                PlayerSpawnService playerSpawnService,
                FactoryFacade factory
            )
        {
            _serverService = serverService;
            _stateService = StateServiceSingleton.GetInstance();
            _playerSpawnService = playerSpawnService;
            _factory = factory;
        }

        public void Init()
        {
            _serverService.On<int>("SetId", _stateService.SetMainPlayerId);
            _serverService.On<EntityState>("InitializeOpponent", _playerSpawnService.SpawnOpponent);
            _serverService.On<EntityState>("UpdatePosition", _stateService.UpdateState);
            _serverService.On<int>("RemoveEntity", _stateService.RemoveEntity);
            _serverService.On<string>("ReceiveChatMessage", PrintReceivedMessage);
            _serverService.On<IEnumerable<NpcState>>("AddNpcs", (states) =>
            {
                _stateService.WriteWithLock(entities =>
                {
                    foreach (var state in states)
                    {
                        if (entities.ContainsKey(state.Id))
                        {
                            (entities[state.Id].ControlComponent as RemoteControlComponent)?.UpdateState(state);
                        }
                        else
                        {
                            entities.Add(state.Id, _factory.GetNpc(state, state.Type));
                        }

                        var idsToRemove = entities
                            .Where(e => e.Value is Npc)
                            .Select(e => e.Key)
                            .Except(states.Select(s => s.Id));

                        foreach (var id in idsToRemove)
                        {
                            entities.Remove(id);
                        }
                    }
                });
            });
            _serverService.On<IEnumerable<NpcState>>("UpdateNpcs", (states) =>
            {
                _stateService.WriteWithLock(entities =>
                {
                    foreach (var state in states)
                    {
                        if (entities.TryGetValue(state.Id, out var entity))
                        {
                            (entity.ControlComponent as RemoteControlComponent)?.UpdateState(state);
                        }
                        else
                        {
                            entities.Add(state.Id, _factory.GetNpc(state, state.Type));
                        }
                    }
                });
            });
            _serverService.On<IEnumerable<PlayerState>>("UpdatePlayers", states =>
            {
                _stateService.WriteWithLock(entities =>
                {
                    foreach (var playerState in states)
                    {
                        if (entities.TryGetValue(playerState.Id, out var playerEntity))
                        {
                            var manager = playerEntity.GetComponent<RemotePlayerPowerupManager>();
                            if (manager is object)
                            {
                                manager.UpdatedPowerups = playerState.Powerups;
                            }
                        }
                    }
                });
            });

            _serverService.StartConnection();
        }

        public void Run()
        {
            Task.Run(async () =>
            {
                while (true)
                {
                    Thread.Sleep(10);

                    await Tick();
                }
            });
        }

        private async Task Tick()
        {
            await _serverService.UpdatePosition(_stateService.GetMainPlayerState());
        }

        private void PrintReceivedMessage(string message)
        {
            System.Console.WriteLine(message);
        }
    }
}
