﻿using System;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Npc;
using TrollMarioClient.Entities.Obstacles;

namespace TrollMarioClient.Loggers
{
    public class HashLogger : IVisitor
    {
        public void Visit(Koopa koopa)
        {
            Console.WriteLine(koopa.GetHashCode());
        }

        public void Visit(Goomba goomba)
        {
            Console.WriteLine(goomba.GetHashCode());
        }

        public void Visit(Projectile projectile)
        {
            Console.WriteLine(projectile.GetHashCode());
        }

        public void Visit(GhostBlock ghostBlock)
        {
            Console.WriteLine(ghostBlock.GetHashCode());
        }

        public void Visit(Obstacle obstacle)
        {
            Console.WriteLine(obstacle.GetHashCode());
        }

        public void Visit(Vines vines)
        {
            Console.WriteLine(vines.GetHashCode());
        }

        public void Visit(WoodenSpikes woodenSpikes)
        {
            Console.WriteLine(woodenSpikes.GetHashCode());
        }

        public void Visit(Player player)
        {
            Console.WriteLine(player.GetHashCode());
        }

        public void Visit(Tile tile)
        {
            Console.WriteLine(tile.GetHashCode());
        }
    }
}
