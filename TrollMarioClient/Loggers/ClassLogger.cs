﻿using System;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Npc;
using TrollMarioClient.Entities.Obstacles;

namespace TrollMarioClient.Loggers
{
    public class ClassLogger : IVisitor
    {
        public void Visit(Koopa koopa)
        {
            Console.WriteLine(koopa.GetType().Name);
        }

        public void Visit(Goomba goomba)
        {
            Console.WriteLine(goomba.GetType().Name);
        }

        public void Visit(Projectile projectile)
        {
            Console.WriteLine(projectile.GetType().Name);
        }

        public void Visit(GhostBlock ghostBlock)
        {
            Console.WriteLine(ghostBlock.GetType().Name);
        }

        public void Visit(Obstacle obstacle)
        {
            Console.WriteLine(obstacle.GetType().Name);
        }

        public void Visit(Vines vines)
        {
            Console.WriteLine(vines.GetType().Name);
        }

        public void Visit(WoodenSpikes woodenSpikes)
        {
            Console.WriteLine(woodenSpikes.GetType().Name);
        }

        public void Visit(Player player)
        {
            Console.WriteLine(player.GetType().Name);
        }

        public void Visit(Tile tile)
        {
            Console.WriteLine(tile.GetType().Name);
        }
    }
}
