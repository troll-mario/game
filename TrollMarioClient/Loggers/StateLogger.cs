﻿using System;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Npc;
using TrollMarioClient.Entities.Obstacles;

namespace TrollMarioClient.Loggers
{
    public class StateLogger : IVisitor
    {
        public void Visit(Koopa koopa)
        {
            Console.WriteLine(koopa.GetState());
        }

        public void Visit(Goomba goomba)
        {
            Console.WriteLine(goomba.GetState());
        }

        public void Visit(Projectile projectile)
        {
            Console.WriteLine(projectile.GetState());
        }

        public void Visit(GhostBlock ghostBlock)
        {
            Console.WriteLine(ghostBlock.GetState());
        }

        public void Visit(Obstacle obstacle)
        {
            Console.WriteLine(obstacle.GetState());
        }

        public void Visit(Vines vines)
        {
            Console.WriteLine(vines.GetState());
        }

        public void Visit(WoodenSpikes woodenSpikes)
        {
            Console.WriteLine(woodenSpikes.GetState());
        }

        public void Visit(Player player)
        {
            Console.WriteLine(player.GetState());
        }

        public void Visit(Tile tile)
        {
            Console.WriteLine(tile.GetState());
        }
    }
}
