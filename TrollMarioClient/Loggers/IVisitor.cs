﻿using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Npc;
using TrollMarioClient.Entities.Obstacles;

namespace TrollMarioClient.Loggers
{
    public interface IVisitor
    {
        void Visit(Koopa koopa);

        void Visit(Goomba goomba);

        void Visit(Projectile projectile);

        void Visit(GhostBlock ghostBlock);

        void Visit(Obstacle obstacle);

        void Visit(Vines vines);

        void Visit(WoodenSpikes woodenSpikes);

        void Visit(Player player);

        void Visit(Tile tile);
    }
}
