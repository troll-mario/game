﻿using SFML.Graphics;
using SFML.System;
using System;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Components.Collider;
using TrollMarioClient.Entities.Obstacles;
using TrollMarioClient.Enums;
using TrollMarioClient.Providers;
using TrollMarioClient.Services;

namespace TrollMarioClient.Factories
{
    public class VisibleObstacleFactory : ObstacleAbstractFactory
    {
        private readonly CollisionService _collisionService;
        private readonly IdProvider _idProvider;
        private readonly TextureFactory _textureFactory;

        public VisibleObstacleFactory(CollisionService collisionService, IdProvider idProvider, TextureFactory textureFactory)
        {
            _collisionService = collisionService;
            _idProvider = idProvider;
            _textureFactory = textureFactory;
        }

        public override GameEntity GetObstacle(string type, Vector2f position)
        {
            switch (type)
            {
                case "Spikes":
                    {
                        var texture = _textureFactory.GetContainer(TextureKey.Spikes);
                        var result = new Spikes(position, texture, new VisibleObstacleState()) { Id = _idProvider.Next() };
                        result.DrawComponent.Color = Color.White;
                        result.DrawComponent.SetDrawingLayer(DrawingLayers.Obstacle);
                        //result.SetVisibility(true);
                        result.Id = _idProvider.Next();
                        result.AddComponent(new StopColliderComponent(_collisionService)
                        {
                            Boundaries = new FloatRect(result.PositionX, result.PositionY + (50f - Spikes.SpikeHeight),
                                result.Width, Spikes.SpikeHeight)
                        });
                        return result;
                    }
                case "WoodenSpikes":
                    {
                        var texture = _textureFactory.GetContainer(TextureKey.WoodenSpikes);
                        var result = new WoodenSpikes(position, texture, new VisibleObstacleState()) { Id = _idProvider.Next() };
                        result.DrawComponent.Color = Color.White;
                        result.DrawComponent.SetDrawingLayer(DrawingLayers.Obstacle);
                        //result.SetVisibility(true);
                        result.Id = _idProvider.Next();
                        var collider = new StopColliderComponent(_collisionService)
                        {
                            Boundaries = new FloatRect(result.PositionX, result.PositionY + (50f - WoodenSpikes.SpikeHeight),
                                result.Width, WoodenSpikes.SpikeHeight)
                        };

                        result.AddComponent(collider);
                        return result;
                    }
                case "Vines":
                    {
                        var texture = _textureFactory.GetContainer(TextureKey.Vines);
                        var result = new Vines(position, texture, new VisibleObstacleState()) { Id = _idProvider.Next() };
                        result.DrawComponent.Color = Color.White;
                        result.DrawComponent.SetDrawingLayer(DrawingLayers.Obstacle);
                        //result.SetVisibility(true);
                        result.AddComponent(new SlowDownColliderComponent(_collisionService));

                        return result;
                    }
                case "GhostBlock":
                    {
                        var texture = _textureFactory.GetContainer(TextureKey.Ground);

                        Obstacle result = new GhostBlock(position, texture, new VisibleObstacleState()) { Id = _idProvider.Next() };
                        result.DrawComponent.Color = Color.White;
                        result.DrawComponent.SetDrawingLayer(DrawingLayers.Obstacle);
                        //result.SetVisibility(true);
                        result.AddComponent(new StopColliderComponent(_collisionService));
                        //result.SetObstacleState(new GhostObstacleState(result));

                        return result;
                    }
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
