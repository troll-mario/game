﻿using SFML.Graphics;
using SFML.System;
using System;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Components.Collider;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Entities.Components.Powerups;
using TrollMarioClient.Enums;
using TrollMarioClient.Providers;
using TrollMarioClient.Services;

namespace TrollMarioClient.Factories
{
    public class PowerupFactory
    {
        private readonly CollisionService _collisionService;
        private readonly IdProvider _idProvider;
        private readonly TextureFactory _textureFactory;

        public PowerupFactory(CollisionService collisionService,
            IdProvider idProvider,
            TextureFactory textureFactory)
        {
            _collisionService = collisionService;
            _idProvider = idProvider;
            _textureFactory = textureFactory;
        }

        private const int ShooterDuration = 15;
        private const int ShieldDuration = 10;
        private const int StarDuration = 10;
        public Tile GetCollectiblePowerup(Vector2f position, BlockType type)
        {
            var drawComponent = new DrawComponent
            {
                TextureContainer = _textureFactory.GetContainer(TextureKey.QuestionBlock)
            };
            drawComponent.SetDrawingLayer(DrawingLayers.Obstacle);

            var collectible = new Tile(position)
            {
                Id = _idProvider.Next()
            };

            collectible.AddComponent(drawComponent);
            collectible.AddComponent(ResolveCollectibleFromType(type));
            collectible.AddComponent(new StopColliderComponent(_collisionService));

            return collectible;
        }

        private static PowerupCollectibleComponent ResolveCollectibleFromType(BlockType type)
        {

            Func<IPowerup, IPowerup> wrap = type switch
            {
                BlockType.ShooterPowerup => existingPowerup => new ShooterPowerup(ShooterDuration, existingPowerup),
                BlockType.ShieldPowerup => existingPowerup => new ShieldPowerup(ShieldDuration, existingPowerup),
                BlockType.StarPowerup => existingPowerup => new StarPowerup(StarDuration, existingPowerup),
                _ => throw new InvalidOperationException()
            };

            return new PowerupCollectibleComponent(wrap);
        }
    }
}