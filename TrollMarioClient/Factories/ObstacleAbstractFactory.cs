﻿using TrollMarioClient.Entities;

namespace TrollMarioClient.Factories
{
    public abstract class ObstacleAbstractFactory
    {
        public abstract GameEntity GetObstacle(string type, SFML.System.Vector2f position);
    }
}
