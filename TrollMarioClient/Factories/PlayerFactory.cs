﻿using SFML.Graphics;
using SFML.System;
using System;
using TrollMarioClient.Builders.Player;
using TrollMarioClient.Entities;
using TrollMarioClient.Enums;
using Unity;

namespace TrollMarioClient.Factories
{
    public class PlayerFactory
    {
        private readonly IUnityContainer _container;

        public PlayerFactory(IUnityContainer container)
        {
            _container = container;
        }

        public Player CreatePlayer(ControlType type, Vector2f position)
        {
            var builder = ResolveBuilder(type);

            builder.Start(position)
                .AddDrawing(Color.White)
                .AddControl()
                .AddColisions()
                .AddPowerupSupport()
                .AddHealth()
                .AddCheckpoints();

            return builder.Build();
        }

        private PlayerBuilder ResolveBuilder(ControlType controlType)
        {
            return controlType switch
            {
                ControlType.Local => _container.Resolve<LocalControlPlayerBuilder>(),
                ControlType.Remote => _container.Resolve<RemoteControlPlayerBuilder>(),
                _ => throw new ArgumentOutOfRangeException(nameof(controlType), controlType, null)
            };
        }
    }
}
