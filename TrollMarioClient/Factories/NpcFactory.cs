﻿using SFML.Graphics;
using SFML.System;
using System;
using TrollMarioClient.Entities.Components.Collider;
using TrollMarioClient.Entities.Components.Control;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Entities.Components.Movement;
using TrollMarioClient.Entities.Npc;
using TrollMarioClient.Enums;
using TrollMarioClient.Services;
using TrollMarioDomain.Dto;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Factories
{
    public class NpcFactory
    {
        private readonly CollisionService _collisionService;
        private readonly TextureFactory _textureFactory;

        public NpcFactory(CollisionService collisionService, TextureFactory textureFactory)
        {
            _collisionService = collisionService;
            _textureFactory = textureFactory;
        }

        public Npc GetNpc(EntityState state, NpcType type)
        {
            return type switch
            {
                NpcType.Goomba => GetGoomba(state),
                NpcType.Koopa => GetKoopa(state),
                NpcType.Projectile => GetProjectile(state),
                _ => throw new NotImplementedException()
            };
        }

        private Goomba GetGoomba(EntityState state)
        {
            var goomba = new Goomba(new Vector2f(state.PositionX ?? 0, state.PositionY ?? 0));

            goomba.AddComponent(new DrawComponent());

            goomba.DrawComponent.Color = Color.White;
            goomba.DrawComponent.SetDrawingLayer(DrawingLayers.Npc);

            var movementComponent = new RemoteMovementComponent(new Vector2f(state.VelocityX ?? 0, state.VelocityY ?? 0));

            goomba.AddComponent(movementComponent);
            goomba.AddComponent(new RemoteControlComponent());
            goomba.AddComponent(new StopColliderComponent(_collisionService));

            return goomba;
        }

        private Koopa GetKoopa(EntityState state)
        {
            var koopa = new Koopa(new Vector2f(state.PositionX ?? 0, state.PositionY ?? 0));

            koopa.AddComponent(new DrawComponent());

            koopa.DrawComponent.Color = Color.White;
            koopa.DrawComponent.SetDrawingLayer(DrawingLayers.Npc);

            var movementComponent = new RemoteMovementComponent(new Vector2f(state.VelocityX ?? 0, state.VelocityY ?? 0));

            koopa.AddComponent(movementComponent);
            koopa.AddComponent(new RemoteControlComponent());
            koopa.AddComponent(new StopColliderComponent(_collisionService));

            return koopa;
        }

        private Projectile GetProjectile(EntityState state)
        {
            var textureContainer = _textureFactory.GetContainer(TextureKey.Projectile);

            var projectile = new Projectile(new Vector2f(state.PositionX.Value, state.PositionY.Value))
            {
                Height = textureContainer.Texture.Size.X,
                Width = textureContainer.Texture.Size.Y
            };

            projectile.AddComponent(new DrawComponent
            {
                Color = Color.White,
                Height = projectile.Height,
                TextureContainer = textureContainer,
                Width = projectile.Width
            });

            var movementComponent = new RemoteMovementComponent(new Vector2f(state.VelocityX.Value, state.VelocityY.Value));

            projectile.AddComponent(movementComponent);
            projectile.AddComponent(new RemoteControlComponent());
            projectile.AddComponent(new PassThroughColliderComponent(_collisionService));

            return projectile;
        }
    }
}