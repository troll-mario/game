﻿using System;
using SFML.System;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Npc;
using TrollMarioClient.Enums;
using TrollMarioClient.Loggers;
using TrollMarioDomain.Dto;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Factories
{
    public class FactoryFacade
    {
        private readonly NpcFactory _npcFactory;
        private readonly ObstacleTileFactoryAdapter _obstacleFactory;
        private readonly PlayerFactory _playerFactory;
        private readonly PowerupFactory _powerupFactory;
        private readonly TileFactory _tileFactory;

        public FactoryFacade
            (
                NpcFactory npcFactory,
                ObstacleTileFactoryAdapter obstacleFactory,
                PlayerFactory playerFactory,
                PowerupFactory powerupFactory,
                TileFactory tileFactory
            )
        {
            _npcFactory = npcFactory;
            _obstacleFactory = obstacleFactory;
            _playerFactory = playerFactory;
            _powerupFactory = powerupFactory;
            _tileFactory = tileFactory;
        }

        public Player CreatePlayer(ControlType type, Vector2f position)
        {
            return _playerFactory.CreatePlayer(type, position);
        }

        public Tile CreateTile(Vector2f position, BlockType type)
        {
            var tile = type switch
            {
                BlockType.Background or BlockType.Ground or BlockType.Bricks => _tileFactory.CreateTile(position, type),
                BlockType.ShooterPowerup or BlockType.ShieldPowerup or BlockType.StarPowerup => _powerupFactory.GetCollectiblePowerup(position, type),
                BlockType.VisibleVines or BlockType.VisibleSpikes or BlockType.VisibleWoodenSpikes or BlockType.InvisibleVines or BlockType.InvisibleSpikes or BlockType.InvisibleWoodenSpikes or BlockType.GhostBlock => _obstacleFactory.CreateTile(position, type),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null),
            };

            tile.Accept(new ClassLogger());
            tile.Accept(new HashLogger());

            return tile;
        }

        public Npc GetNpc(EntityState state, NpcType type)
        {
            return _npcFactory.GetNpc(state, type);
        }
    }
}
