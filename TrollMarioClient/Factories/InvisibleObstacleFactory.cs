﻿using SFML.Graphics;
using System;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Components.Collider;
using TrollMarioClient.Entities.Obstacles;
using TrollMarioClient.Enums;
using TrollMarioClient.Providers;
using TrollMarioClient.Services;

namespace TrollMarioClient.Factories
{
    public class InvisibleObstacleFactory : ObstacleAbstractFactory
    {
        private readonly CollisionService _collisionService;
        private readonly TextureFactory _textureFactory;
        private readonly IdProvider _idProvider;

        public InvisibleObstacleFactory(CollisionService collisionService, IdProvider idProvider,
            TextureFactory textureFactory)
        {
            _collisionService = collisionService;
            _idProvider = idProvider;
            _textureFactory = textureFactory;
        }

        public override GameEntity GetObstacle(string type, SFML.System.Vector2f position)
        {
            switch (type)
            {
                case "Spikes":
                    {
                        var texture = _textureFactory.GetContainer(TextureKey.Spikes);
                        var result = new Spikes(position, texture, new InvisibleObstacleState()) { Id = _idProvider.Next() };

                        //result.SetVisibility(false);
                        result.DrawComponent.Color = Color.Transparent;
                        result.DrawComponent.SetDrawingLayer(DrawingLayers.Obstacle);
                        result.AddComponent(new StopColliderComponent(_collisionService)
                        {
                            Boundaries = new FloatRect(result.PositionX, result.PositionY + (50f - Spikes.SpikeHeight),
                                result.Width, Spikes.SpikeHeight)
                        });

                        return result;
                    }
                case "Vines":
                    {
                        var texture = _textureFactory.GetContainer(TextureKey.Vines);
                        var result = new Vines(position, texture, new InvisibleObstacleState());
                        //result.SetVisibility(false);

                        result.DrawComponent.Color = Color.Transparent;
                        result.DrawComponent.SetDrawingLayer(DrawingLayers.Obstacle);

                        result.Id = _idProvider.Next();
                        result.AddComponent(new SlowDownColliderComponent(_collisionService));

                        return result;
                    }
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
