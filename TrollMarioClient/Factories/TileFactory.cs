﻿using SFML.Graphics;
using SFML.System;
using System.Collections.Generic;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Components.Collider;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Entities.Components.Drawing.GameTextures;
using TrollMarioClient.Enums;
using TrollMarioClient.Providers;
using TrollMarioClient.Services;

namespace TrollMarioClient.Factories
{
    public class TileFactory
    {
        private protected readonly CollisionService _collisionService;
        private protected readonly IdProvider _idProvider;
        private readonly TextureFactory _textureFactory;
        private readonly Dictionary<BlockType, Tile> _tileDictionary;

        public TileFactory(CollisionService collisionService, IdProvider idProvider, TextureFactory textureFactory)
        {
            _collisionService = collisionService;
            _idProvider = idProvider;
            _textureFactory = textureFactory;
            _tileDictionary = new Dictionary<BlockType, Tile>();
        }

        public virtual Tile CreateTile(Vector2f position, BlockType type)
        {
            if (_tileDictionary.ContainsKey(type))
            {
                var clonedTile = _tileDictionary[type].Clone(_idProvider.Next(), position);
                clonedTile.DrawComponent.SetDrawingLayer(DrawingLayers.Static);             //-- Cloning does not catch this.
                return clonedTile as Tile;
            }

            var tile = new Tile(position)
            {
                Id = _idProvider.Next()
            };

            var drawingComponent = new DrawComponent
            {
                TextureContainer = ResolveTexture(type)
            };
            drawingComponent.SetDrawingLayer(DrawingLayers.Static);

            tile.AddComponent(drawingComponent);


            if (type != BlockType.Background)
            {
                tile.AddComponent(new StopColliderComponent(_collisionService));
            }

            _tileDictionary.Add(type, tile);

            return tile;
        }

        private IGameTextureContainer ResolveTexture(BlockType type)
        {
            return type switch
            {
                BlockType.Bricks => _textureFactory.GetContainer(TextureKey.Bricks),
                BlockType.Ground => _textureFactory.GetContainer(TextureKey.Ground),
                _ => null
            };
        }
    }
}