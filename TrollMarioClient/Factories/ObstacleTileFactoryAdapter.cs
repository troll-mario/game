﻿using SFML.System;
using System;
using TrollMarioClient.Entities;
using TrollMarioClient.Enums;
using TrollMarioClient.Providers;
using TrollMarioClient.Services;

namespace TrollMarioClient.Factories
{
    public class ObstacleTileFactoryAdapter : TileFactory
    {
        private readonly InvisibleObstacleFactory _invisibleObstacleFactory;
        private readonly VisibleObstacleFactory _visibleObstacleFactory;

        public ObstacleTileFactoryAdapter
            (
                CollisionService collisionService,
                IdProvider idProvider,
                TextureFactory textureFactory,
                InvisibleObstacleFactory invisibleObstacleFactory,
                VisibleObstacleFactory visibleObstacleFactory
            )
            : base
            (
                collisionService,
                idProvider,
                textureFactory
            )
        {
            _invisibleObstacleFactory = invisibleObstacleFactory;
            _visibleObstacleFactory = visibleObstacleFactory;
        }

        public override Tile CreateTile(Vector2f position, BlockType type)
        {
            return type switch
            {
                BlockType.VisibleVines => (Tile)_visibleObstacleFactory.GetObstacle("Vines", position),
                BlockType.VisibleSpikes => (Tile)_visibleObstacleFactory.GetObstacle("Spikes", position),
                BlockType.VisibleWoodenSpikes => (Tile)_visibleObstacleFactory.GetObstacle("WoodenSpikes", position),
                BlockType.InvisibleVines => (Tile)_invisibleObstacleFactory.GetObstacle("Vines", position),
                BlockType.InvisibleSpikes => (Tile)_invisibleObstacleFactory.GetObstacle("Spikes", position),
                BlockType.InvisibleWoodenSpikes => (Tile)_invisibleObstacleFactory.GetObstacle("WoodenSpikes", position),
                BlockType.GhostBlock => (Tile)_visibleObstacleFactory.GetObstacle("GhostBlock", position),
                _ => throw new NotImplementedException()
            };
        }
    }
}
