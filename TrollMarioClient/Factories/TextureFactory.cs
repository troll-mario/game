﻿using System;
using System.Collections.Generic;
using SFML.Graphics;
using TrollMarioClient.Entities.Components.Drawing.GameTextures;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Factories
{
    public class TextureFactory
    {
        private readonly Dictionary<TextureKey, IGameTextureContainer> _containers;

        public TextureFactory()
        {
            _containers = new Dictionary<TextureKey, IGameTextureContainer>();
        }

        public IGameTextureContainer GetContainer(TextureKey key)
        {
            if (!_containers.TryGetValue(key, out var container))
            {
                container = ResolveContainer(key);
                _containers.Add(key, container);
            }

            return container;
        }

        private static IGameTextureContainer ResolveContainer(TextureKey key)
        {
            return key switch
            {
                TextureKey.Mario => new MarioTextureContainer(),
                TextureKey.Spikes => new SpikesTextureContainer(),
                TextureKey.WoodenSpikes => new WoodenSpikesTextureContainer(),
                TextureKey.Vines => new VinesTextureContainer(),
                TextureKey.QuestionBlock => new QuestionBlockTextureContainer(),
                TextureKey.Bricks => new BricksTextureContainer(),
                TextureKey.Ground => new GroundTextureContainer(),
                TextureKey.Projectile => new ProjectileTextureContainer(),
                _ => throw new ArgumentOutOfRangeException(nameof(key), key, null)
            };
        }
    }
}
