﻿using System.Reflection.Metadata.Ecma335;
using SFML.System;
using TrollMarioClient.Entities.Interfaces;

namespace TrollMarioClient.Entities.Components.Movement
{
    public class RemoteMovementComponent : IComponent
    {
        private GameEntity _entity;

        private object _positionLock;
        private float? _positionX;
        private float? _positionY;

        private float _alphaX;
        private float _alphaY;
        private object _velocityLock;
        private float _currentVelocityX;
        private float _currentVelocityY;
        private float _newVelocityX;
        private float _newVelocityY;
        private float _oldVelocityX;
        private float _oldVelocityY;

        public RemoteMovementComponent() { }

        public RemoteMovementComponent(Vector2f velocity)
        {
            _oldVelocityX = _currentVelocityX = _newVelocityX = velocity.X;
            _oldVelocityY = _currentVelocityY = _newVelocityY = velocity.Y;
        }

        public void Init(GameEntity entity)
        {
            _alphaX = 0.6f;
            _alphaY = 0.05f;
            _entity = entity;
            _positionLock = new object();
            _velocityLock = new object();

            _entity.Events.SetPosition += SetPosition;
            _entity.Events.SetVelocity += SetVelocity;
            _entity.Events.MoveResult += SetPositionByOffset;

        }

        public void SetPosition(object sender, Vector2f position)
        {
            lock (_positionLock)
            {
                _positionX = position.X;
                _positionY = position.Y;
            }
        }

        public void SetVelocity(object sender, Vector2f velocity)
        {
            lock (_velocityLock)
            {
                _oldVelocityX = _newVelocityX;
                _oldVelocityY = _newVelocityY;

                _currentVelocityX = _newVelocityX = velocity.X;
                _currentVelocityY = _newVelocityY = velocity.Y;
            }
        }

        public void Update()
        {
            UpdatePosition();
            UpdateVelocity();
        }

        private void SetPositionByOffset(object sender, Vector2f offset)
        {
            _entity.PositionX += offset.X;
            _entity.PositionY += offset.Y;
        }

        private void UpdatePosition()
        {
            lock (_positionLock)
            {
                if (_positionX.HasValue && _positionY.HasValue)
                {
                    _entity.Events.InvokeTryMove(new Vector2f(
                        _positionX.Value - _entity.PositionX,
                        _positionY.Value - _entity.PositionY
                    ));

                    _positionX = default;
                    _positionY = default;
                }
                else
                {
                    lock (_velocityLock)
                    {
                        _entity.Events.InvokeTryMove(new Vector2f(_currentVelocityX, _currentVelocityY));
                    }
                }
            }
        }

        private void UpdateVelocity()
        {
            lock (_velocityLock)
            {
                _currentVelocityX += (_newVelocityX - _oldVelocityX) * _alphaX;
                _currentVelocityY += (_newVelocityY - _oldVelocityY) * _alphaY;
            }
        }

        public object Clone()
        {
            var clone = new RemoteMovementComponent();
            return clone;
        }
    }
}