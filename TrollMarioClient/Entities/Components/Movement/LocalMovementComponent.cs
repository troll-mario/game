﻿using SFML.System;
using System;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Entities.Interfaces;
using TrollMarioClient.Enums;
using TrollMarioClient.Providers;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Entities.Components.Movement
{
    public class LocalMovementComponent : IComponent
    {
        public JumpState JumpState { get; set; } = JumpState.NotStarted;

        public float VelocityX
        {
            get => _velocityX;
            set => _velocityX = VelocityWithinLimit(value);
        }

        public float VelocityY
        {
            get => _velocityY;
            set
            {
                if (JumpState == JumpState.NotStarted ||
                    JumpState == JumpState.FirstSecondEnabled)
                {
                    var newVelocity = -value;
                    _velocityY = VelocityWithinLimit(newVelocity);

                    JumpState = JumpState == JumpState.NotStarted ? JumpState.FirstSecondDisabled : JumpState.Second;
                }
            }
        }

        private GameEntity _entity;
        private float _velocityX;
        private float _velocityY;
        private float _maxY;
        private float _maxX;

        private const float JumpIncrement = 15f;
        private const float WalkIncrement = 10f;
        private const float HorizontalVelocityDecay = 0.7f;
        private const float VerticalVelocityDecay = 2f;
        private const float MaxVelocity = 15f;

        public void Init(GameEntity entity)
        {
            _entity = entity;
            _entity.Events.MoveResult += UpdatePosition;
            _entity.Events.Collision += OnCollision;
            _entity.Events.SetPosition += SetPosition;
            _entity.Events.SetJumpState += OnJumpStateChange;
            _entity.Events.IncreaseVelocity += OnIncreaseVelocity;
            _maxY = GameWindowProvider.Height - _entity.Height;
            _maxX = GameWindowProvider.Width - _entity.Width;
        }

        private void OnIncreaseVelocity(object sender, Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    VelocityY += JumpIncrement;
                    break;
                case Direction.Right:
                    VelocityX += WalkIncrement;
                    break;
                case Direction.Down:
                    break;
                case Direction.Left:
                    VelocityX -= WalkIncrement;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        private void OnJumpStateChange(object sender, JumpState state)
        {
            JumpState = state;
        }

        private void OnCollision(object sender, CollisionInfo info)
        {
            if (info.Side == Direction.Down)
            {
                _entity.Events.InvokeJumpStateChange(JumpState.NotStarted);
            }
        }

        private void SetPosition(object sender, Vector2f position)
        {
            _entity.PositionX = position.X;
            _entity.PositionY = position.Y;
        }

        private void UpdatePosition(object sender, Vector2f offset)
        {
            _entity.PositionX += offset.X;
            _entity.PositionY += offset.Y;

            if (offset.Y == 0)
            {
                _velocityY = 0;
            }

            _entity.PositionX = Math.Min(_maxX, _entity.PositionX);
            _entity.PositionX = Math.Max(_entity.PositionX, 0);

            _entity.PositionY = Math.Min(_maxY, _entity.PositionY);
            _entity.PositionY = Math.Max(_entity.PositionY, 0);
        }

        public void Update()
        {
            TryMove();
            ReduceVelocity();
        }

        private void ReduceVelocity()
        {
            _velocityX *= HorizontalVelocityDecay;
            if (Math.Abs(VelocityX) < 5)
            {
                _velocityX = 0;
            }

            _velocityY += VerticalVelocityDecay;
            if (_entity.PositionY >= _maxY)
            {
                _velocityY = 0;
                JumpState = JumpState.NotStarted;
            }
        }

        private void TryMove()
        {
            if (_velocityY != 0 || _velocityX != 0)
            {
                var positionOffset = new Vector2f(VelocityX, VelocityY);
                _entity.Events.InvokeTryMove(positionOffset);
            }
        }

        private static float VelocityWithinLimit(float value)
        {
            if (value < -MaxVelocity)
            {
                return -MaxVelocity;
            }

            if (value > MaxVelocity)
            {
                return MaxVelocity;
            }

            return value;
        }

        public object Clone()
        {
            var clone = new LocalMovementComponent
            {
                VelocityX = VelocityX,
                VelocityY = VelocityY,
                _maxX = _maxX,
                _maxY = _maxY
            };
            return clone;
        }
    }
}
