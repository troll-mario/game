﻿using SFML.System;
using System;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Entities.Interfaces;
using TrollMarioClient.Loggers;

namespace TrollMarioClient.Entities.Components.Checkpoints
{
    class CheckpointComponent : IComponent
    {
        private Player _player;
        private CommandInvoker _invoker;

        public void Init(GameEntity entity)
        {
            _player = entity as Player ?? throw new NotSupportedException("Checkpoint Component can only be added to Player class entities");
            _invoker = new CommandInvoker();
            _player.Events.Collision += OnCollision;

            var startingCheckpoint = new AddCheckpoint(_player, new Vector2f(0, 0));
            _invoker.AddCommand(startingCheckpoint);
            _invoker.Execute();
        }


        public void Update()
        {
            //Add checkpoints at certain positions


            //Load checkpoint
            if (_player.PositionY > 650)
            {
                _player.Accept(new StateLogger());

                var command = new LoadCheckpoint(_player);
                _invoker.AddCommand(command);
                _invoker.Execute();
            }
        }

        public void OnCollision(object sender, CollisionInfo info)
        {
            //Dummy code, loading checkpoints should occur after bumping into obstacles or when Y coordinate is below map
            //if (false)
            //{
            //    var command = new LoadCheckpoint(_player);
            //    _invoker.AddCommand(command);
            //    _invoker.Execute();
            //}
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
