﻿using TrollMarioClient.Entities.Interfaces;

namespace TrollMarioClient.Entities.Components.Health
{
    public interface IHealthComponent : IComponent
    {
        public int Health { get; set; }
    }
}