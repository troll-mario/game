﻿using System;

namespace TrollMarioClient.Entities.Components.Health
{
    public class HealthComponent : IHealthComponent
    {
        private GameEntity _entity;
        public int Health { get; set; }
        public void Init(GameEntity entity)
        {
            _entity = entity;
            Health = 1;
            _entity.Events.ChangeHealth += OnChangeHealth;
        }

        private void OnChangeHealth(object sender, int amount)
        {
            Console.WriteLine(amount);
            Health += amount;
            if (Health <= 0)
            {
                _entity.Events.InvokeDeath();
            }

            Console.WriteLine(Health);
        }

        public void Update()
        {
        }

        public object Clone()
        {
            throw new System.NotImplementedException();
        }
    }
}