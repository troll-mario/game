﻿using SFML.Graphics;
using TrollMarioClient.Entities.Components.Drawing.GameTextures;
using TrollMarioClient.Entities.Interfaces;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing
{
    public interface IDrawComponent : IComponent
    {
        public Color Color { get; set; }
        public Drawable Drawable { get; }

        public void SetTextureContainer(IGameTextureContainer container);
        public void UpdateTexture();

        public void SetDrawingLayer(DrawingLayers layer);
        public DrawingLayers GetDrawingLayer();
    }
}
