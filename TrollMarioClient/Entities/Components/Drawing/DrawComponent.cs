﻿using SFML.Graphics;
using SFML.System;
using TrollMarioClient.Entities.Components.Drawing.GameTextures;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing
{
    public class DrawComponent : IDrawComponent
    {
        public Drawable Drawable => _shape;
        private DrawingLayers _layer = DrawingLayers.Default;

        public float Height { get; set; }
        public float Width { get; set; }
        public Color Color
        {
            get => _shape?.FillColor ?? Color.Transparent;
            set
            {
                if (_shape is object)
                {
                    _shape.FillColor = value;
                }
            }
        }
        public IGameTextureContainer TextureContainer { get; set; }

        private GameEntity _entity;
        private Shape _shape;

        public DrawComponent()
        {
            Height = 50;
            Width = 50;
        }

        public void Init(GameEntity entity)
        {
            _entity = entity;

            _shape = new RectangleShape
            {
                FillColor = TextureContainer is object ? Color.White : Color.Transparent,
                Position = new Vector2f(entity.PositionX, entity.PositionY),
                Size = new Vector2f(Height, Width),
                Texture = TextureContainer?.Texture
            };
        }

        public void Update()
        {
            _shape.Position = new Vector2f(_entity.PositionX, _entity.PositionY);
        }

        public void UpdateTexture()
        {
            _shape.Texture = TextureContainer?.Texture;
        }

        public void SetDrawingLayer(DrawingLayers layer)
        {
            this._layer = layer;
        }

        public DrawingLayers GetDrawingLayer()
        {
            return this._layer;
        }

        public object Clone()
        {
            var clone = new DrawComponent
            {
                Width = Width,
                Height = Height,
                Color = Color
            };
            if (TextureContainer != null)
                clone.TextureContainer = TextureContainer;

            return clone;
        }

        public void SetTextureContainer(IGameTextureContainer container)
        {
            this.TextureContainer = container;
        }
    }
}
