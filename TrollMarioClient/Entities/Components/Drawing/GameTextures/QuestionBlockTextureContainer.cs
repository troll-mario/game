﻿using SFML.Graphics;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing.GameTextures
{
    public class QuestionBlockTextureContainer : IGameTextureContainer
    {
        public Texture Texture { get; }

        public QuestionBlockTextureContainer()
        {
            Texture = TextureKey.QuestionBlock.Generate();
        }
    }
}