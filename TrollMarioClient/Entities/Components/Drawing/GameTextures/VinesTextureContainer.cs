﻿using SFML.Graphics;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing.GameTextures
{
    public class VinesTextureContainer : IGameTextureContainer
    {
        public Texture Texture { get; }

        public VinesTextureContainer()
        {
            Texture = TextureKey.Vines.Generate();
        }
    }
}