﻿using SFML.Graphics;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing.GameTextures
{
    class ProjectileTextureContainer : IGameTextureContainer
    {
        public Texture Texture => TextureKey.Projectile.Generate();
    }
}
