﻿using SFML.Graphics;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing.GameTextures
{
    public static class TextureExtensions
    {
        public static Texture Generate(this TextureKey key)
        {
            return new Texture($"../../../Sprites/{key}.png");
        }
    }
}