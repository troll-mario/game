﻿using SFML.Graphics;

namespace TrollMarioClient.Entities.Components.Drawing.GameTextures
{
    public interface IGameTextureContainer
    {
        public Texture Texture { get; }
    }
}