﻿using SFML.Graphics;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing.GameTextures
{
    public class SpikesTextureContainer : IGameTextureContainer
    {
        public Texture Texture { get; }

        public SpikesTextureContainer()
        {
            Texture = TextureKey.Spikes.Generate();
        }
    }
}