﻿using SFML.Graphics;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing.GameTextures
{
    public class GroundTextureContainer : IGameTextureContainer
    {
        public Texture Texture { get; }

        public GroundTextureContainer()
        {
            Texture = TextureKey.Ground.Generate();
        }
    }
}