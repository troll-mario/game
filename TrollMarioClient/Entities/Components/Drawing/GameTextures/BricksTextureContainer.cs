﻿using SFML.Graphics;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing.GameTextures
{
    public class BricksTextureContainer : IGameTextureContainer
    {
        public Texture Texture { get; }

        public BricksTextureContainer()
        {
            Texture = TextureKey.Bricks.Generate();
        }
    }
}