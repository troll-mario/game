﻿using SFML.Graphics;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing.GameTextures
{
    public class MarioTextureContainer : IGameTextureContainer
    {
        public Texture Texture { get; }

        public MarioTextureContainer()
        {
            Texture = TextureKey.Mario.Generate();
        }
    }
}
