﻿using SFML.Graphics;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing.GameTextures
{
    public class BrokenWoodenSpikesTextureContainer : IGameTextureContainer
    {
        public Texture Texture { get; }

        public BrokenWoodenSpikesTextureContainer()
        {
            Texture = TextureKey.BrokenWoodenSpikes.Generate();
        }
    }
}