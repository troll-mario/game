﻿using SFML.Graphics;
using TrollMarioClient.Enums;

namespace TrollMarioClient.Entities.Components.Drawing.GameTextures
{
    public class WoodenSpikesTextureContainer : IGameTextureContainer
    {
        public Texture Texture { get; }

        public WoodenSpikesTextureContainer()
        {
            Texture = TextureKey.WoodenSpikes.Generate();
        }
    }
}