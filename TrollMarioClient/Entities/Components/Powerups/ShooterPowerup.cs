﻿using SFML.Window;
using TrollMarioClient.Providers;
using TrollMarioClient.Services;
using TrollMarioClient.Utilities;
using TrollMarioDomain.Enums;
using Unity;

namespace TrollMarioClient.Entities.Components.Powerups
{
    public class ShooterPowerup : BasePowerupComponent
    {
        private GameEntity _entity;
        private Direction _shootingDirection = Direction.Right;
        public ShooterPowerup(double duration, IPowerup child) : base(duration, child)
        {
        }

        public override void Initialize(GameEntity entity)
        {
            base.Initialize(entity);

            _entity = entity;
        }

        public override void Execute()
        {
            UpdateShootingDirection();

            if (Keyboard.IsKeyPressed(Keyboard.Key.Space))
            {
                Shoot();
            }

            Child.Execute();
        }

        private void UpdateShootingDirection()
        {
            if (KeyboardUtilities.IsDirectionPressed(Direction.Left))
            {
                _shootingDirection = Direction.Left;
            }
            else if (KeyboardUtilities.IsDirectionPressed(Direction.Right))
            {
                _shootingDirection = Direction.Right;
            }
        }

        private void Shoot()
        {
            UnityContainerProvider.Container
                .Resolve<ServerService>()
                .Shoot(_entity.GetState(), _shootingDirection)
                .Wait();
        }
    }
}
