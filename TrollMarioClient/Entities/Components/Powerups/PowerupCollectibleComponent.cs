﻿using System;
using TrollMarioClient.Entities.Components.Powerups.Managers;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Entities.Interfaces;

namespace TrollMarioClient.Entities.Components.Powerups
{
    internal class PowerupCollectibleComponent : IComponent
    {
        private GameEntity _entity;
        private readonly Func<IPowerup, IPowerup> _decorate;
        public PowerupCollectibleComponent(Func<IPowerup, IPowerup> decorate)
        {
            _decorate = decorate;
        }
        
        public void Init(GameEntity entity)
        {
            _entity = entity;
            _entity.Events.Collision += OnCollision;
        }

        private void OnCollision(object sender, CollisionInfo info)
        {
            if (info.CollidesWith is Player player)
            {
                var manager = player.GetComponent<PowerupManager>();
                manager.Powerup = _decorate(manager.Powerup);
                manager.Powerup.Initialize(player);

                _entity.Events.InvokeDeath();
                _entity.Destroy = true;
            }
        }

        public void Update()
        {
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
