﻿using System;
using System.Threading.Tasks;
using TrollMarioClient.Entities.Components.Powerups.Managers;

namespace TrollMarioClient.Entities.Components.Powerups
{
    public abstract class BasePowerupComponent : IPowerup
    {
        public IPowerup Child { get; set; }
        public bool Expired { get; set; }

        private readonly double _duration;
        private bool _initialized;
        private Player _player;

        protected BasePowerupComponent(double duration, IPowerup child)
        {
            _duration = duration;
            Child = child;
        }

        public virtual void Initialize(GameEntity entity)
        {
            if (!_initialized)
            {
                if (entity is not Player player)
                {
                    throw new NotSupportedException("Powerups only supported by player");
                }

                _player = player;
                Task.Delay(TimeSpan.FromSeconds(_duration)).ContinueWith(_ => Unwrap());
                _initialized = true;
            }
        }

        public virtual void Execute() { }

        protected virtual void Unwrap()
        {
            Expired = true;
            var manager = _player.GetComponent<PowerupManager>();
            if (manager.Powerup == this)
            {
                manager.Powerup = Child;
            }
            else
            {
                manager.RemoveExpiredPowerups();
            }
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
