﻿using System;
using TrollMarioClient.Entities.Components.Health;

namespace TrollMarioClient.Entities.Components.Powerups
{
    public class StarPowerup : BasePowerupComponent
    {
        public StarPowerup(double duration, IPowerup child) : base(duration, child)
        {
        }

        private GameEntity _entity;
        private HealthComponent _healthComponent;
        private int _initialHealth;
        private const int StarHealthOffset = 30000;

        public override void Initialize(GameEntity entity)
        {
            base.Initialize(entity);
            _healthComponent = entity.GetComponent<HealthComponent>();
            _initialHealth = _healthComponent.Health;
            _entity = entity;
            _entity.Events.InvokeChangeHealth(_entity, StarHealthOffset);
        }

        protected override void Unwrap()
        {
            var healthDiff = _initialHealth - (_healthComponent.Health - StarHealthOffset);
            var alterBy = healthDiff - StarHealthOffset;
            _entity.Events.InvokeChangeHealth(_entity, alterBy);
            base.Unwrap();
        }

        public override void Execute()
        {
            Console.WriteLine("STAR");
            Child.Execute();
        }
    }
}