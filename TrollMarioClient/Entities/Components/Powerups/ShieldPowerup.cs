﻿using System;
using TrollMarioClient.Entities.Components.Health;

namespace TrollMarioClient.Entities.Components.Powerups
{
    public class ShieldPowerup : BasePowerupComponent
    {
        private GameEntity _entity;
        private HealthComponent _healthComponent;
        private int _initialHealth;
        private bool _disabled;
        public ShieldPowerup(double duration, IPowerup child) : base(duration, child)
        {
        }

        public override void Initialize(GameEntity entity)
        {
            base.Initialize(entity);
            _healthComponent = entity.GetComponent<HealthComponent>();
            _initialHealth = _healthComponent.Health;
            _entity = entity;
            _entity.Events.InvokeChangeHealth(_entity, 1);
        }

        public override void Execute()
        {
            if (_healthComponent.Health <= _initialHealth && !_disabled)
            {
                Disable();
            }
            else
            {
                Console.WriteLine("SHIELD");
            }

            Child.Execute();
        }

        private void Disable()
        {
            // update sprite
            _disabled = true;
        }

        protected override void Unwrap()
        {
            if (!_disabled)
            {
                Disable();
                _entity.Events.InvokeChangeHealth(_entity, -1);
            }
            base.Unwrap();
        }
    }
}