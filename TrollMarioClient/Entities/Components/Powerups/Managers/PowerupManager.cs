﻿using System;
using System.Collections.Generic;
using TrollMarioClient.Entities.Interfaces;

namespace TrollMarioClient.Entities.Components.Powerups.Managers
{
    public abstract class PowerupManager : IComponent
    {
        public IPowerup Powerup;
        protected Player Player;
        public void Init(GameEntity entity)
        {
            if (!(entity is Player player))
            {
                throw new NotSupportedException("Powerups only supported by player");
            }

            Player = player;
            Powerup = new PowerupStubComponent();
        }

        public virtual void Update()
        {
            Powerup.Execute();
        }

        protected abstract bool FilterBy(IPowerup powerup);

        public IList<IPowerup> RemoveExpiredPowerups()
        {
            var powerups = new Stack<IPowerup>();
            while (!(Powerup is PowerupStubComponent))
            {
                powerups.Push(Powerup);
                Powerup = Powerup.Child;
            }

            var expired = new List<IPowerup>();

            while (powerups.TryPop(out var powerup))
            {
                if (FilterBy(powerup))
                {
                    powerup.Child = Powerup;
                    Powerup = powerup;
                }
                else
                {
                    expired.Add(powerup);
                }
            }

            return expired;
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}