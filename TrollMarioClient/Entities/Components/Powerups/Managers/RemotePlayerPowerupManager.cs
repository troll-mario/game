﻿using System.Collections.Generic;
using System.Linq;

namespace TrollMarioClient.Entities.Components.Powerups.Managers
{
    public class RemotePlayerPowerupManager : PowerupManager
    {
        public ICollection<string> UpdatedPowerups { get; set; }
        public RemotePlayerPowerupManager()
        {
            UpdatedPowerups = new List<string>();
        }

        public override void Update()
        {
            if (UpdatedPowerups.Any())
            {
                RemoveExpiredPowerups();
                UpdatedPowerups = new List<string>();
            }

            base.Update();
        }

        protected override bool FilterBy(IPowerup powerup)
        {
            return UpdatedPowerups.Contains(powerup.GetType().ToString());
        }
    }
}