﻿namespace TrollMarioClient.Entities.Components.Powerups.Managers
{
    public class LocalPlayerPowerupManager : PowerupManager
    {
        protected override bool FilterBy(IPowerup powerup)
        {
            return !powerup.Expired;
        }
    }
}