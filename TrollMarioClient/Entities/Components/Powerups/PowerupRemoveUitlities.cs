﻿using System.Collections.Generic;
namespace TrollMarioClient.Entities.Components.Powerups
{
    public static class PowerupRemoveUitlities
    {
        public static IPowerup RemoveExpiredPowerups(this IPowerup powerupComponent)
        {
            var currentPowerup = powerupComponent;
            var powerups = new Stack<IPowerup>();
            while (!(currentPowerup is PowerupStubComponent))
            {
                powerups.Push(currentPowerup);
                currentPowerup = currentPowerup.Child;
            }

            while (powerups.TryPop(out var powerup))
            {
                if (!powerup.Expired)
                {
                    powerup.Child = currentPowerup;
                    currentPowerup = powerup;
                }
            }

            return currentPowerup;
        }
    }
}