﻿namespace TrollMarioClient.Entities.Components.Powerups
{
    // only for pattern sake, have something to decorate.
    public class PowerupStubComponent : IPowerup
    {
        public IPowerup Child { get; set; }

        public bool Expired { get; set; }

        public void Initialize(GameEntity entity)
        {
        }

        public void Execute()
        {
        }
    }
}
