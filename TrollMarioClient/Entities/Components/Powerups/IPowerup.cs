﻿namespace TrollMarioClient.Entities.Components.Powerups
{
    public interface IPowerup
    {
        public IPowerup Child { get; set; }
        public bool Expired { get; }
        public void Initialize(GameEntity entity);
        public void Execute();
    }
}