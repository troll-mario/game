﻿using SFML.System;
using TrollMarioDomain.Dto;

namespace TrollMarioClient.Entities.Components.Control
{
    public class RemoteControlComponent : IControlComponent
    {
        private GameEntity _entity;

        public void UpdateState(EntityState state)
        {
            if (state.HasPosition)
            {
                _entity.Events.InvokeSetPosition(new Vector2f(state.PositionX.Value, state.PositionY.Value));
            }

            if (state.HasVelocity)
            {
                _entity.Events.InvokeSetVelocity(new Vector2f(state.VelocityX.Value, state.VelocityY.Value));
            }
        }

        public void Init(GameEntity entity)
        {
            _entity = entity;
        }

        public void Update() { }

        public object Clone()
        {
            var clone = new RemoteControlComponent();
            return clone;
        }
    }
}