﻿using SFML.Graphics;
using SFML.Window;
using TrollMarioClient.Entities.Components.Movement;
using TrollMarioClient.Enums;
using TrollMarioClient.Providers;
using TrollMarioClient.Utilities;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Entities.Components.Control
{
    public class LocalControlComponent : IControlComponent
    {
        private readonly RenderWindow _window = GameWindowProvider.Window;
        private GameEntity _entity;
        private JumpState _jumpState = JumpState.NotStarted;

        public void Init(GameEntity entity)
        {
            _window.KeyReleased += (sender, args) => OnKeyUp(args.Code);
            _entity = entity;
            _entity.Events.SetJumpState += (sender, state) => _jumpState = state;
        }

        public void Update()
        {
            CheckKeys();
        }

        public void CheckKeys()
        {
            if (!_window.HasFocus())
            {
                return;
            }

            if (KeyboardUtilities.IsDirectionPressed(Direction.Left))
            {
                _entity.Events.InvokeIncreaseVelocity(Direction.Left);
            }

            if (KeyboardUtilities.IsDirectionPressed(Direction.Right))
            {
                _entity.Events.InvokeIncreaseVelocity(Direction.Right);
            }

            if (KeyboardUtilities.IsDirectionPressed(Direction.Up))
            {
                _entity.Events.InvokeIncreaseVelocity(Direction.Up);
            }
        }

        public void OnKeyUp(Keyboard.Key key)
        {
            switch (key)
            {
                case Keyboard.Key.W:
                case Keyboard.Key.Up:
                    if (_jumpState == JumpState.FirstSecondDisabled)
                    {
                       _entity.Events.InvokeJumpStateChange(JumpState.FirstSecondEnabled);
                    }
                    break;
            }
        }

        public object Clone()
        {
            var clone = new LocalControlComponent();
            return clone;
        }
    }
}