﻿using SFML.Graphics;
using SFML.System;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Enums.Extensions;
using TrollMarioClient.Services;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Entities.Components.Collider
{
    public class PassThroughColliderComponent : ColliderComponentBase
    {
        public PassThroughColliderComponent(CollisionService collisionService) : base(collisionService) { }

        public sealed override (Vector2f, Direction?, bool) AdjustOffset(Vector2f offset, FloatRect boundaries, FloatRect otherBoundaries)
        {
            return (offset, null, boundaries.Intersects(otherBoundaries));
        }

        public override object Clone()
        {
            return new PassThroughColliderComponent(_collisionService);
        }

        protected sealed override void Notify(Direction? direction, ColliderComponentBase other)
        {
            Notify(new CollisionInfo
            {
                CollidesWith = other.Entity,
                Side = direction
            });

            other.Notify(new CollisionInfo
            {
                CollidesWith = Entity,
                Side = direction.Invert()
            });
        }
    }
}
