﻿using SFML.Graphics;
using SFML.System;
using System;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Entities.Interfaces;
using TrollMarioClient.Services;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Entities.Components.Collider
{
    public abstract class ColliderComponentBase : IComponent
    {
        public FloatRect Boundaries
        {
            get => _boundaries;
            set
            {
                _customBoundaries = true;
                _boundaries = value;
            }
        }
        public GameEntity Entity { get; private set; }

        protected readonly CollisionService _collisionService;

        private FloatRect _boundaries;
        private bool _customBoundaries;

        public ColliderComponentBase(CollisionService collisionService)
        {
            _collisionService = collisionService;
        }

        public abstract (Vector2f, Direction?, bool) AdjustOffset(Vector2f offset, FloatRect boundaries, FloatRect otherBoundaries);

        public abstract object Clone();

        public void Init(GameEntity entity)
        {
            Entity = entity;
            _collisionService.Register(this);

            if (!_customBoundaries)
            {
                Boundaries = new FloatRect(Entity.GetPosition(), new Vector2f(Entity.Width, Entity.Height));
            }

            Entity.Events.TryMove += OnTryMove;
            Entity.Events.MoveResult += MoveCollider;
            Entity.Events.Death += OnDeath;
            Entity.Events.SetPosition += SetNewColliderPosition;
        }

        #region Template

        public Vector2f TryCollide(Vector2f offset, ColliderComponentBase other)
        {
            var (offsetAdjusted, direction, notify) = other.AdjustOffset(offset, Boundaries, other.Boundaries);

            offset.X = Math.Abs(offsetAdjusted.X) < Math.Abs(offset.X) ? offsetAdjusted.X : offset.X;
            offset.Y = Math.Abs(offsetAdjusted.Y) < Math.Abs(offset.Y) ? offsetAdjusted.Y : offset.Y;

            if (notify)
            {
                Notify(direction, other);
            }

            return offset;
        }

        #endregion

        public void Update() { }

        #region Template

        protected abstract void Notify(Direction? direction, ColliderComponentBase other);

        protected static float RemoveCalculationError(float calculationResult)
        {
            if (Math.Abs(calculationResult) < 0.1)
            {
                calculationResult = 0;
            }

            return calculationResult;
        }

        #endregion

        private void OnDeath(object sender, EventArgs eventArgs)
        {
            _collisionService.Unregister(this);
        }

        private void MoveCollider(object sender, Vector2f offset)
        {
            Boundaries = new FloatRect(
                Boundaries.Left + offset.X,
                Boundaries.Top + offset.Y,
                Boundaries.Width,
                Boundaries.Height
            );
        }

        private void SetNewColliderPosition(object sender, Vector2f position)
        {
            Boundaries = new FloatRect(position, new Vector2f(Boundaries.Width, Boundaries.Height));
        }

        private void OnTryMove(object sender, Vector2f offset)
        {
            Entity.Events.InvokeMoveResult(_collisionService.TryMove(this, offset));
        }

        public void Notify(CollisionInfo info)
        {
            Entity.Events.InvokeCollision(info);
        }
    }
}