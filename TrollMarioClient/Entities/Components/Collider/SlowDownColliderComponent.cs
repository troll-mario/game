﻿using SFML.Graphics;
using SFML.System;
using System;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Enums.Extensions;
using TrollMarioClient.Services;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Entities.Components.Collider
{
    public sealed class SlowDownColliderComponent : ColliderComponentBase
    {
        private const float _minOffset = 0.2f;
        private const float _slowDownCoeficient = 0.2f;

        public SlowDownColliderComponent(CollisionService collisionService) : base(collisionService) { }

        public sealed override (Vector2f, Direction?, bool) AdjustOffset(Vector2f offset, FloatRect boundaries, FloatRect otherBoundaries)
        {
            if (boundaries.Intersects(otherBoundaries))
            {
                var offsetAdjusted = new Vector2f();

                if (offset.X > 0)
                {
                    offsetAdjusted.X = Math.Max(RemoveCalculationError(offset.X * _slowDownCoeficient), _minOffset);
                }
                else if (offset.X < 0)
                {
                    offsetAdjusted.X = Math.Min(RemoveCalculationError(offset.X * _slowDownCoeficient), -_minOffset);
                }

                if (offset.Y > 0)
                {
                    offsetAdjusted.Y = Math.Max(RemoveCalculationError(offset.Y * _slowDownCoeficient), _minOffset);
                }
                else if (offset.Y < 0)
                {
                    offsetAdjusted.Y = Math.Min(RemoveCalculationError(offset.Y * _slowDownCoeficient), -_minOffset);
                }

                return (offsetAdjusted, null, true);
            }

            return (offset, null, false);
        }

        public override object Clone()
        {
            return new StopColliderComponent(_collisionService);
        }

        protected sealed override void Notify(Direction? direction, ColliderComponentBase other)
        {
            Notify(new CollisionInfo
            {
                CollidesWith = other.Entity,
                Side = direction
            });

            other.Notify(new CollisionInfo
            {
                CollidesWith = Entity,
                Side = direction.Invert()
            });
        }
    }
}
