﻿using SFML.Graphics;
using SFML.System;
using System;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Enums.Extensions;
using TrollMarioClient.Services;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Entities.Components.Collider
{
    public sealed class StopColliderComponent : ColliderComponentBase
    {
        public StopColliderComponent(CollisionService collisionService) : base(collisionService) { }

        public sealed override (Vector2f, Direction?, bool) AdjustOffset(Vector2f offset, FloatRect boundaries, FloatRect otherBoundaries)
        {
            var movedBoundaries = new FloatRect(
                boundaries.Left + offset.X,
                boundaries.Top + offset.Y,
                boundaries.Width,
                boundaries.Height
            );

            if (movedBoundaries.Intersects(otherBoundaries))
            {
                var horizontalDirection = offset.X < 0 ? Direction.Left : Direction.Right;
                var verticalDirection = offset.Y < 0 ? Direction.Up : Direction.Down;

                var horizontalOverlap =
                    Math.Min(movedBoundaries.Left + movedBoundaries.Width, otherBoundaries.Left + otherBoundaries.Width) -
                    Math.Max(movedBoundaries.Left, otherBoundaries.Left);

                var verticalOverlap =
                    Math.Min(movedBoundaries.Top + movedBoundaries.Height, otherBoundaries.Top + otherBoundaries.Height) -
                    Math.Max(movedBoundaries.Top, otherBoundaries.Top);

                var direction = verticalOverlap > horizontalOverlap ? horizontalDirection : verticalDirection;

                var offsetAdjusted = direction switch
                {
                    Direction.Up => new Vector2f(
                        offset.X,
                        RemoveCalculationError(offset.Y + (otherBoundaries.Top + otherBoundaries.Height - movedBoundaries.Top))
                    ),
                    Direction.Right => new Vector2f(
                        RemoveCalculationError(offset.X - (movedBoundaries.Left + movedBoundaries.Width - otherBoundaries.Left)),
                        offset.Y
                    ),
                    Direction.Down => new Vector2f(
                        offset.X,
                        RemoveCalculationError(offset.Y - (movedBoundaries.Top + movedBoundaries.Height - otherBoundaries.Top))
                    ),
                    Direction.Left => new Vector2f(
                        RemoveCalculationError(offset.X - (movedBoundaries.Left - otherBoundaries.Left - otherBoundaries.Width)),
                        offset.Y
                    ),
                    _ => throw new ArgumentOutOfRangeException(nameof(direction), direction, null)
                };

                return (offsetAdjusted, direction, true);
            }

            return (offset, null, false);
        }

        public override object Clone()
        {
            return new StopColliderComponent(_collisionService);
        }

        protected sealed override void Notify(Direction? direction, ColliderComponentBase other)
        {
            Notify(new CollisionInfo
            {
                CollidesWith = other.Entity,
                Side = direction
            });

            other.Notify(new CollisionInfo
            {
                CollidesWith = Entity,
                Side = direction.Invert()
            });
        }
    }
}
