﻿using System;

namespace TrollMarioClient.Entities.Interfaces
{
    public interface IComponent : ICloneable
    {
        public void Init(GameEntity entity);
        public void Update();
    }
}
