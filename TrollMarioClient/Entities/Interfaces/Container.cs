﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrollMarioClient.Entities.Interfaces
{
    public interface Container<T> : IEnumerable<T>
    { 

    }
}
