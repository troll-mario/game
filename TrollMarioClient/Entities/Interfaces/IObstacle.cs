﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrollMarioClient.Entities.Obstacles;

namespace TrollMarioClient.Entities.Interfaces
{
    public interface IObstacle
    {
        public void SetObstacleState(BaseObstacleState state);
        public BaseObstacleState GetObstacleState();
        public bool IsComposite();
    }
}
