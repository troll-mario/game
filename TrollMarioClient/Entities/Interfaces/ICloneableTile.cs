﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrollMarioClient.Entities
{
    interface ICloneableTile
    {
        public Tile Clone(int newId, SFML.System.Vector2f newPosition);
    }
}
