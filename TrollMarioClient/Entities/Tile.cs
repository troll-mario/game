﻿using SFML.System;
using TrollMarioClient.Entities.Events;
using TrollMarioClient.Entities.Interfaces;
using TrollMarioClient.Loggers;
using TrollMarioClient.Utilities;

namespace TrollMarioClient.Entities
{
    public class Tile : GameEntity, ICloneableTile
    {
        public Tile(Vector2f position) : base(position)
        {

        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        public Tile Clone(int newId, Vector2f newPosition)
        {
            var clone = new Tile(newPosition)
            {
                Id = newId,
                Events = (EntityEvents)Events.Clone(),
                Height = Height,
                Width = Width,
                Destroy = Destroy
            };

            ConcreteIterator<IComponent> cloneIterator = Components.GetEnumerator();
            cloneIterator.Reset();
            while (cloneIterator.HasNext())
            {
                var c = cloneIterator.Next() as IComponent;
                IComponent clonedComponent = (IComponent)c.Clone();
                clone.AddComponent(clonedComponent);
            }

            return clone;
        }
    }
}
