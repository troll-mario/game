﻿using SFML.System;
using System;
using TrollMarioClient.Entities.Events.Models;

namespace TrollMarioClient.Entities.Npc
{
    public abstract class Npc : GameEntity
    {
        public Npc(Vector2f position) : base(position)
        {
            Events.Collision += OnCollision;
        }

        protected virtual void OnCollision(object sender, CollisionInfo info)
        {
            throw new NotImplementedException();
        }
    }
}
