﻿using SFML.System;
using TrollMarioClient.Loggers;

namespace TrollMarioClient.Entities.Npc
{
    public class Koopa : Npc
    {
        public Koopa(Vector2f position) : base(position) { }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
