﻿using SFML.System;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Loggers;

namespace TrollMarioClient.Entities.Npc
{
    public class Projectile : Npc
    {
        public Projectile(Vector2f position) : base(position) { }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        protected override void OnCollision(object sender, CollisionInfo info) { }
    }
}
