﻿using SFML.System;
using TrollMarioClient.Loggers;

namespace TrollMarioClient.Entities.Npc
{
    public class Goomba : Npc
    {
        public Goomba(Vector2f position) : base(position) { }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
