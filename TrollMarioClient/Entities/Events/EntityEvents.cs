﻿using SFML.System;
using System;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Enums;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Entities.Events
{
    public class EntityEvents : IEntityEvents
    {
        public event EventHandler<CollisionInfo> Collision;
        public event EventHandler<SpawnRequest> SpawnRequest;
        public event EventHandler Death;
        public event EventHandler<Vector2f> TryMove;
        public event EventHandler<Vector2f> MoveResult;
        public event EventHandler<int> ChangeHealth;
        public event EventHandler<Vector2f> SetPosition;
        public event EventHandler<JumpState> SetJumpState;
        public event EventHandler<Direction> IncreaseVelocity;
        public event EventHandler<Vector2f> SetVelocity;

        public void InvokeIncreaseVelocity(Direction direction)
        {
            IncreaseVelocity?.Invoke(this, direction);
        }

        public void InvokeSetPosition(Vector2f position)
        {
            SetPosition?.Invoke(this, position);
        }

        public void InvokeJumpStateChange(JumpState state)
        {
            SetJumpState?.Invoke(this, state);
        }

        public void InvokeCollision(CollisionInfo info)
        {
            Collision?.Invoke(this, info);
        }

        public void InvokeDeath()
        {
            Death?.Invoke(this, EventArgs.Empty);
        }

        public void InvokeSpawnRequest(SpawnRequest request)
        {
            SpawnRequest?.Invoke(this, request);
        }

        public void InvokeTryMove(Vector2f offset)
        {
            TryMove?.Invoke(this, offset);
        }

        public void InvokeMoveResult(Vector2f offset)
        {
            MoveResult?.Invoke(this, offset);
        }

        public void InvokeChangeHealth(GameEntity sender, int amount)
        {

            ChangeHealth?.Invoke(sender, amount);
        }

        public void InvokeCheckpoint(Vector2f position)
        {
            SetPosition?.Invoke(this, position);
        }

        public void InvokeSetVelocity(Vector2f velocity)
        {
            SetVelocity?.Invoke(this, velocity);
        }

        public object Clone()
        {
            return new EntityEvents
            {
                Collision = Collision,
                SpawnRequest = SpawnRequest,
                Death = Death,
                TryMove = TryMove,
                MoveResult = MoveResult,
                SetPosition = SetPosition,
                ChangeHealth = ChangeHealth
            };
        }
    }
}
