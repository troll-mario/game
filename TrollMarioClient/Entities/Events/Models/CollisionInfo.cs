﻿using TrollMarioDomain.Enums;

namespace TrollMarioClient.Entities.Events.Models
{
    public struct CollisionInfo
    {
        public GameEntity CollidesWith { get; set; }
        public Direction? Side { get; set; }
    }
}
