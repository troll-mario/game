﻿using SFML.System;

namespace TrollMarioClient.Entities.Events.Models
{
    public class SpawnRequest
    {
        public string Key { get; set; }
        public Vector2f? Position { get; set; }
    }
}