﻿using SFML.System;
using System;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Enums;
using TrollMarioClient.Providers;
using TrollMarioClient.Services;
using TrollMarioDomain.Dto;
using TrollMarioDomain.Enums;
using Unity;

namespace TrollMarioClient.Entities.Events
{
    public class RemoteEventsProxy : IEntityEvents
    {
        private readonly IEntityEvents _proxyTo;
        private readonly GameEntity _entity;
        private ServerService _serverService;

        public RemoteEventsProxy(GameEntity entity)
        {
            _entity = entity;
            _proxyTo = new EntityEvents();
        }

        public event EventHandler<CollisionInfo> Collision
        {
            add => _proxyTo.Collision += value;
            remove => _proxyTo.Collision -= value;
        }

        public event EventHandler<SpawnRequest> SpawnRequest
        {
            add => _proxyTo.SpawnRequest += value;
            remove => _proxyTo.SpawnRequest -= value;
        }

        public event EventHandler Death
        {
            add => _proxyTo.Death += value;
            remove => _proxyTo.Death -= value;
        }

        public event EventHandler<Vector2f> TryMove
        {
            add => _proxyTo.TryMove += value;
            remove => _proxyTo.TryMove -= value;
        }

        public event EventHandler<Vector2f> MoveResult
        {
            add => _proxyTo.MoveResult += value;
            remove => _proxyTo.MoveResult -= value;
        }

        public event EventHandler<int> ChangeHealth
        {
            add => _proxyTo.ChangeHealth += value;
            remove => _proxyTo.ChangeHealth -= value;
        }

        public event EventHandler<Vector2f> SetPosition
        {
            add => _proxyTo.SetPosition += value;
            remove => _proxyTo.SetPosition -= value;
        }

        public event EventHandler<JumpState> SetJumpState
        {
            add => _proxyTo.SetJumpState += value;
            remove => _proxyTo.SetJumpState -= value;
        }

        public event EventHandler<Direction> IncreaseVelocity
        {
            add => _proxyTo.IncreaseVelocity += value;
            remove => _proxyTo.IncreaseVelocity -= value;
        }

        public event EventHandler<Vector2f> SetVelocity
        {
            add => _proxyTo.SetVelocity += value;
            remove => _proxyTo.SetVelocity -= value;
        }

        public void InvokeIncreaseVelocity(Direction direction)
        {
            _proxyTo?.InvokeIncreaseVelocity(direction);
        }

        public void InvokeSetPosition(Vector2f position)
        {
            _proxyTo?.InvokeSetPosition(position);
        }

        public void InvokeJumpStateChange(JumpState state)
        {
            _proxyTo.InvokeJumpStateChange(state);
        }

        public void InvokeCollision(CollisionInfo info)
        {
            _proxyTo.InvokeCollision(info);
        }

        public void InvokeDeath()
        {
            _proxyTo.InvokeDeath();
        }

        public void InvokeSpawnRequest(SpawnRequest request)
        {
            _proxyTo.InvokeSpawnRequest(request);
        }

        public void InvokeTryMove(Vector2f offset)
        {
            _proxyTo.InvokeTryMove(offset);
        }

        public void InvokeMoveResult(Vector2f offset)
        {
            _proxyTo.InvokeMoveResult(offset);
        }

        public void InvokeChangeHealth(GameEntity sender, int amount)
        {
            _serverService ??= UnityContainerProvider.Container.Resolve<ServerService>();
            var healthUpdate = new HealthUpdate
            {
                Amount = amount,
                DealerId = sender.Id,
                ReceiverId = _entity.Id
            };

            var customListenerName = $"{nameof(_serverService.UpdateHealth)}_{_entity.Id}";
            _serverService.On(customListenerName, (int confirmedAmount) =>
            {
                _proxyTo.InvokeChangeHealth(sender, confirmedAmount);
                _serverService.DisposeOnListener(customListenerName);
            });

            _serverService.UpdateHealth(healthUpdate).GetAwaiter().GetResult();
        }

        public void InvokeCheckpoint(Vector2f position)
        {
            _proxyTo.InvokeCheckpoint(position);
        }

        public void InvokeSetVelocity(Vector2f velocity)
        {
            _proxyTo.InvokeSetVelocity(velocity);
        }

        public object Clone()
        {
            return _proxyTo.Clone();
        }
    }
}