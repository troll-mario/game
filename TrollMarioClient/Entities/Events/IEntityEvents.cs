﻿using SFML.System;
using System;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Enums;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Entities.Events
{
    public interface IEntityEvents
    {
        event EventHandler<CollisionInfo> Collision;
        event EventHandler<SpawnRequest> SpawnRequest;
        event EventHandler Death;
        event EventHandler<Vector2f> TryMove;
        event EventHandler<Vector2f> MoveResult;
        event EventHandler<int> ChangeHealth;
        event EventHandler<Vector2f> SetPosition;
        event EventHandler<JumpState> SetJumpState;
        event EventHandler<Direction> IncreaseVelocity;
        event EventHandler<Vector2f> SetVelocity;

        void InvokeIncreaseVelocity(Direction direction);
        void InvokeSetPosition(Vector2f position);
        void InvokeJumpStateChange(JumpState state);
        void InvokeCollision(CollisionInfo info);
        void InvokeDeath();
        void InvokeSpawnRequest(SpawnRequest request);
        void InvokeTryMove(Vector2f offset);
        void InvokeMoveResult(Vector2f offset);
        void InvokeChangeHealth(GameEntity sender, int amount);
        void InvokeCheckpoint(Vector2f position);
        void InvokeSetVelocity(Vector2f velocity);
        object Clone();
    }
}