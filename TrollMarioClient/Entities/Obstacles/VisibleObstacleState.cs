﻿using SFML.Graphics;
using TrollMarioClient.Entities.Components.Collider;
using TrollMarioClient.Entities.Components.Drawing.GameTextures;

namespace TrollMarioClient.Entities.Obstacles
{
    class VisibleObstacleState : BaseObstacleState
    {
        public VisibleObstacleState() : base(true, false)
        {
        }


        public override void StateSpecificUpdate(Obstacle parent)
        {
            //Do nothing
        }

        public override void OnCollision(Obstacle parentObstacle)
        {


            if (parentObstacle.GetType() == typeof(WoodenSpikes))
            {
                parentObstacle.DrawComponent.SetTextureContainer(new BrokenWoodenSpikesTextureContainer());
                parentObstacle.DrawComponent.UpdateTexture();

                parentObstacle.SetObstacleState(new DestroyedObstacleState(parentObstacle));
                parentObstacle.GetComponent<ColliderComponentBase>().Boundaries = new FloatRect(parentObstacle.PositionX, parentObstacle.PositionY + (50f - 20),
                    parentObstacle.Width, 20);
            }
            else if (parentObstacle.GetType() == typeof(GhostBlock))
            {
                parentObstacle.SetObstacleState(new GhostObstacleState(parentObstacle));
            }
            else
            {
                //Do damage
            }
        }
    }
}
