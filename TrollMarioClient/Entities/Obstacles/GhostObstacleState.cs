﻿using SFML.Graphics;
using TrollMarioClient.Entities.Components.Collider;

namespace TrollMarioClient.Entities.Obstacles
{
    public class GhostObstacleState : BaseObstacleState
    {
        private const int BlinkInterval = 10;

        private int _tickCounter = 0;
        private int _blinkTimer = 0;
        private FloatRect _oldCollider;

        public GhostObstacleState(Obstacle parent) : base(true, false)
        {

            FloatRect temp = parent.GetComponent<ColliderComponentBase>().Boundaries;
            _oldCollider = new FloatRect(temp.Left, temp.Top, temp.Width, temp.Height);
        }


        public override void StateSpecificUpdate(Obstacle parent)
        {
            if (_tickCounter == 20)
            {
                parent.GetComponent<ColliderComponentBase>().Boundaries = new FloatRect(0, 0, 0, 0);
            }
            else if (_tickCounter > 10 && _tickCounter < 500)
            {
                parent.DrawComponent.Color = new SFML.Graphics.Color(255, 255, 255, 50);
            }
            else if (_tickCounter >= 500 && _tickCounter < 750)
            {
                if (_blinkTimer > BlinkInterval)
                {
                    InvertAlpha(parent);
                    _blinkTimer = 0;
                }

                _blinkTimer++;
            }
            else if (_tickCounter >= 750)
            {
                ResetParameters(parent);
                _tickCounter = 0;
            }

            _tickCounter++;
        }


        public override void OnCollision(Obstacle parentObstacle)
        {
            // Do absolutely nothing
        }

        private static void InvertAlpha(GameEntity parentObstacle)
        {
            if (parentObstacle.DrawComponent.Color == new SFML.Graphics.Color(255, 255, 255, 50))
            {
                parentObstacle.DrawComponent.Color = Color.White;
            }
            else
            {
                parentObstacle.DrawComponent.Color = new SFML.Graphics.Color(255, 255, 255, 50);
            }
        }

        private void ResetParameters(Obstacle parent)
        {
            parent.DrawComponent.Color = Color.White;
            parent.GetComponent<ColliderComponentBase>().Boundaries = _oldCollider;
            parent.SetObstacleState(new VisibleObstacleState());
        }
    }
}
