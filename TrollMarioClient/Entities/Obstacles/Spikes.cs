﻿using SFML.Graphics;
using SFML.System;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Entities.Components.Drawing.GameTextures;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Loggers;

namespace TrollMarioClient.Entities.Obstacles
{
    public class Spikes : Obstacle
    {
        //Place for future variables
        public const float SpikeHeight = 20f;
        public Spikes(Vector2f position, IGameTextureContainer container, BaseObstacleState state) : base(position, state)
        {
            Events.Collision += OnCollision;
            Height = 20f;
            var drawComponent = new DrawComponent
            {
                TextureContainer = container
            };
            AddComponent(drawComponent);
            drawComponent.Color = Color.Transparent;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        private void OnCollision(object sender, CollisionInfo info)
        {

            info.CollidesWith.Events.InvokeChangeHealth(this,-1);
            this.GetObstacleState().OnCollision(this);
        }
    }
}
