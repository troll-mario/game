﻿using SFML.Graphics;
using TrollMarioClient.Entities.Components.Collider;
using TrollMarioClient.Entities.Components.Drawing.GameTextures;

namespace TrollMarioClient.Entities.Obstacles
{
    public class DestroyedObstacleState : BaseObstacleState
    {

        private const int BlinkInterval = 10;

        private int _tickCounter = 0;
        private int _blinkTimer = 0;
        private readonly FloatRect _oldCollider;

        public DestroyedObstacleState(Obstacle parentObstacle) : base(true, false)
        {
            _oldCollider = parentObstacle.GetComponent<ColliderComponentBase>().Boundaries;
        }


        public override void StateSpecificUpdate(Obstacle parent)
        {
            if (_tickCounter >= 500 && _tickCounter < 750)
            {
                if (_blinkTimer > BlinkInterval)
                {
                    InvertAlpha(parent);
                    _blinkTimer = 0;
                }

                _blinkTimer++;
            }
            else if (_tickCounter >= 750)
            {
                parent.GetComponent<ColliderComponentBase>().Boundaries = _oldCollider;
                parent.DrawComponent.SetTextureContainer(new WoodenSpikesTextureContainer());
                parent.DrawComponent.UpdateTexture();
                parent.SetObstacleState(new VisibleObstacleState());
            }

            _tickCounter++;
        }

        public override void OnCollision(Obstacle parentObstacle)
        {
            //throw new NotImplementedException();
            // Do absolutely nothing
        }

        private static void InvertAlpha(GameEntity parentObstacle)
        {
            if (parentObstacle.DrawComponent.Color == new SFML.Graphics.Color(255, 255, 255, 50))
            {
                parentObstacle.DrawComponent.Color = Color.White;
                //parentObstacle.DrawComponent.UpdateTexture();
            }
            else
            {
                parentObstacle.DrawComponent.Color = new SFML.Graphics.Color(255, 255, 255, 50);
                //parentObstacle.DrawComponent.UpdateTexture();
            }
        }
    }
}
