﻿using SFML.Graphics;
using SFML.System;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Entities.Components.Drawing.GameTextures;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Loggers;

namespace TrollMarioClient.Entities.Obstacles
{
    public class WoodenSpikes : Obstacle
    {
        //Place for future variables
        public const float SpikeHeight = 40f;
        public WoodenSpikes(Vector2f position, IGameTextureContainer container, BaseObstacleState state) : base(position, state)
        {
            Events.Collision += OnCollision;
            Height = 40f;
            var drawComponent = new DrawComponent
            {
                TextureContainer = container
            };
            AddComponent(drawComponent);
            drawComponent.Color = Color.Transparent;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        private void OnCollision(object sender, CollisionInfo info)
        {
            this.GetObstacleState().OnCollision(this);
        }
    }
}
