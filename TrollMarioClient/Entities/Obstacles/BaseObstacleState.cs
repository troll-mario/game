﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrollMarioClient.Entities.Obstacles
{
    public abstract class BaseObstacleState
    {
        private bool isVisible;
        private bool isMoving;

        public abstract void StateSpecificUpdate(Obstacle parent);

        public BaseObstacleState(bool visibility, bool moving)
        {
            isVisible = visibility;
            isMoving = moving;
        }

        public abstract void OnCollision(Obstacle parentObstacle);


        public bool IsVisible()
        {
            return isVisible;
        }

        public bool IsMoving()
        {
            return isMoving;
        }
    }
}
