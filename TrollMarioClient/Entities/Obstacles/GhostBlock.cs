﻿using SFML.Graphics;
using SFML.System;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Entities.Components.Drawing.GameTextures;
using TrollMarioClient.Entities.Events.Models;

namespace TrollMarioClient.Entities.Obstacles
{
    public class GhostBlock : Obstacle
    {
        //Place for future variables
        public const float SpikeHeight = 20f;
        public GhostBlock(Vector2f position, IGameTextureContainer container, BaseObstacleState state) : base(position, state)
        {
            Events.Collision += OnCollision;
            Height = 20f;
            var drawComponent = new DrawComponent
            {
                TextureContainer = container
            };
            AddComponent(drawComponent);
            drawComponent.Color = Color.Transparent;
        }

        private void OnCollision(object sender, CollisionInfo info)
        {
            this.GetObstacleState().OnCollision(this);
        }
    }
}
