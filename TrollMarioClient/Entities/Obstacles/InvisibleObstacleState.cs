﻿using SFML.Graphics;
using System;

namespace TrollMarioClient.Entities.Obstacles
{
    class InvisibleObstacleState : BaseObstacleState
    {
        public InvisibleObstacleState() : base(false, false)
        {
        }

        public override void StateSpecificUpdate(Obstacle parent)
        {
            //Do nothing
        }

        public override void OnCollision(Obstacle parentObstacle)
        {
            //Do damage
            parentObstacle.DrawComponent.Color = Color.White;
            parentObstacle.SetObstacleState(new VisibleObstacleState());
        }
    }
}
