﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using TrollMarioClient.Entities.Interfaces;

namespace TrollMarioClient.Entities.Obstacles
{
    public class ObstacleDirector : IObstacle 
    {
        private List<IObstacle> ObstacleList = new List<IObstacle>();

        public void SetObstacleState(BaseObstacleState state)
        {
            foreach (var e in ObstacleList)
            {
                e.SetObstacleState(state);

            }
        }

        public BaseObstacleState GetObstacleState()
        {
            throw new NotImplementedException();
        }

        public bool IsComposite()
        {
            return true;
        }

        //This can be moved
        public void Add(IObstacle obstacle)
        {
            ObstacleList.Add(obstacle);
        }
    }
}
