﻿using System.ComponentModel;
using SFML.Graphics;
using SFML.System;
using TrollMarioClient.Loggers;
using TrollMarioClient.Entities.Interfaces;
using TrollMarioClient.Utilities;
using IComponent = TrollMarioClient.Entities.Interfaces.IComponent;

namespace TrollMarioClient.Entities.Obstacles
{
    public class Obstacle : Tile, IObstacle
    {
        BaseObstacleState obstacleState;

        public Obstacle(Vector2f position, BaseObstacleState state) : base(position)
        {
            obstacleState = state;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void Update()
        {
            ConcreteIterator<IComponent> iterator = Components.GetEnumerator();
            iterator.Reset();
            while (iterator.HasNext())
            {
                var c = iterator.Next() as IComponent;
                c.Update();
            }
            this.obstacleState.StateSpecificUpdate(this);
        }

        public void SetObstacleState(BaseObstacleState state)
        {
            this.obstacleState = state;
            if (state.GetType() == typeof(InvisibleObstacleState))
            {
                this.DrawComponent.Color = Color.Transparent;
            }
        }

        public BaseObstacleState GetObstacleState()
        {
            return obstacleState;
        }

        public bool IsComposite()
        {
            return false;
        }
    }
}
