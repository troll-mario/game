﻿using SFML.Graphics;
using SFML.System;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Entities.Components.Drawing.GameTextures;
using TrollMarioClient.Entities.Events.Models;
using TrollMarioClient.Enums;
using TrollMarioClient.Factories;
using TrollMarioClient.Loggers;
using TrollMarioClient.Services;

namespace TrollMarioClient.Entities.Obstacles
{
    public class Vines : Obstacle
    {
        public Vines(Vector2f position, IGameTextureContainer container, BaseObstacleState state) : base(position, state)
        {
            Events.Collision += OnCollision;
            var drawComponent = new DrawComponent
            {
                TextureContainer = container
            };
            AddComponent(drawComponent);
            drawComponent.Color = Color.Transparent;
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        private void OnCollision(object sender, CollisionInfo info)
        {
            this.GetObstacleState().OnCollision(this);
        }
    }
}
