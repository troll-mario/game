﻿namespace TrollMarioClient.Entities.Constants
{
    class NpcConstants
    {
        public const float GoombaSpeed = 4f;
        public const float KoopaSpeed = 5f;
        public const float KoopaShellSpeed = 15f;
    }
}
