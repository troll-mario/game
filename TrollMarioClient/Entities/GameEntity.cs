﻿using SFML.System;
using TrollMarioClient.Entities.Components.Control;
using TrollMarioClient.Entities.Components.Drawing;
using TrollMarioClient.Entities.Components.Health;
using TrollMarioClient.Entities.Components.Movement;
using TrollMarioClient.Entities.Events;
using TrollMarioClient.Entities.Interfaces;
using TrollMarioClient.Loggers;
using TrollMarioClient.Utilities;
using TrollMarioDomain.Dto;

namespace TrollMarioClient.Entities
{
    public abstract class GameEntity
    {
        public int Id { get; set; }
        public IEntityEvents Events { get; protected set; }
        public IControlComponent ControlComponent { get; private set; }
        public IDrawComponent DrawComponent { get; private set; }
        public IHealthComponent HealthComponent { get; private set; }
        public virtual float PositionX { get; set; }
        public virtual float PositionY { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public bool Destroy { get; set; }

        public ConcreteContainer<IComponent> Components = new ConcreteContainer<IComponent>();

        public GameEntity(Vector2f position)
        {
            Height = 50;
            Width = 50;
            Events = Events = new RemoteEventsProxy(this);
            PositionX = position.X;
            PositionY = position.Y;
        }

        public abstract void Accept(IVisitor visitor);

        public virtual void AddComponent(IComponent component)
        {
            switch (component)
            {
                case IDrawComponent drawComponent:
                    DrawComponent = drawComponent;
                    break;
                case IControlComponent controlComponent:
                    ControlComponent = controlComponent;
                    break;
                case IHealthComponent healthComponent:
                    HealthComponent = healthComponent;
                    break;
            }

            component.Init(this);
            Components.Add(component);
        }

        public T GetComponent<T>() where T : class, IComponent
        {
            var component = Components.FirstOrDefault<T>();

            return component;
        }

        public Vector2f GetPosition()
        {
            return new Vector2f(PositionX, PositionY);
        }

        public EntityState GetState()
        {
            var movementComponent = Components.FirstOrDefault<LocalMovementComponent>();

            return new EntityState
            {
                Id = Id,
                PositionX = PositionX,
                PositionY = PositionY,
                VelocityX = movementComponent?.VelocityX,
                VelocityY = movementComponent?.VelocityY,
                Health = HealthComponent.Health
            };
        }

        public virtual void Update()
        {
            ConcreteIterator<IComponent> iterator = Components.GetEnumerator();
            iterator.Reset();
            while (iterator.HasNext())
            {
                var c = iterator.Next() as IComponent;
                c.Update();
            }
        }
    }
}
