﻿using SFML.System;
using TrollMarioClient.Loggers;

namespace TrollMarioClient.Entities
{
    public sealed class Player : GameEntity
    {
        public Vector2f Checkpoint { get; set; }
        public Player(Vector2f position) : base(position) 
        {
            Checkpoint = position;
        }

        public void LoadCheckpoint()
        {
            Events.InvokeCheckpoint(Checkpoint);
        }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
