﻿using System;
using System.Collections;
using System.Collections.Generic;
using TrollMarioClient.Utilities;

namespace TrollMarioClient.Entities.Interfaces
{
    public class ConcreteContainer<T> : Container<T>
    {
        private readonly List<T> _components = new List<T>();

        public int Count
        {
            get { return _components.Count; }
        }

        public void Add(T component)
        {
            _components.Add(component);
        }

        public void Clear()
        {
            _components.Clear();
        }

        public K FirstOrDefault<K>() where K : class
        {
            for (int i = 0; i < _components.Count; i++)
            {
                if (_components[i] is K)
                {
                    return _components[i] as K;
                }
            }
            return null;
        }


        public ConcreteIterator<T> GetEnumerator()
        {
            return new ConcreteIterator<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new ConcreteIterator<T>(this);
        }

        public object this[int index]
        {
            get { return _components[index]; }
        }
    }
}
