﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrollMarioClient
{
    public class CommandInvoker
    {
        private List<Command> _commands;
        private List<Command> _undoCommands;

        public CommandInvoker()
        {
            _commands = new List<Command>();
            _undoCommands = new List<Command>();
        }

        public void AddCommand(Command command)
        {
            _commands.Add(command);
        }

        public void Execute()
        {
            foreach(Command c in _commands)
            {
                Console.WriteLine("Executing command " + c.GetType().ToString());
                c.Execute();
                if(c.undoable)
                    _undoCommands.Add(c);
            }
            _commands.Clear();
        }

        public void Undo()
        {
            if (_undoCommands.Count == 0)
                return;
            Console.WriteLine("Undoing command " + _undoCommands[_undoCommands.Count - 1].GetType().ToString());
            _undoCommands[_undoCommands.Count - 1].Unexecute();
            _undoCommands.RemoveAt(_undoCommands.Count - 1);
        }
    }
}
