﻿using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Entities;

namespace TrollMarioClient
{
    public abstract class Command
    {
        protected GameEntity receiver;
        public bool undoable { get; protected set; } = true;
        public Command(GameEntity receiver)
        {
            this.receiver = receiver;
        }

        public abstract void Execute();

        public abstract void Unexecute();
    }
}
