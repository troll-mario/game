﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Entities;
using TrollMarioClient.Services;

namespace TrollMarioClient
{
    public class AddCheckpoint : Command
    {
        Player _player;
        Vector2f _checkpointPosition;
        Vector2f _previousPosition;
        

        public AddCheckpoint(Player player, Vector2f checkpointPosition) : base(player)
        {
            _player = player;
            _checkpointPosition = checkpointPosition;
        }

        public override void Execute()
        {
            _previousPosition = _player.Checkpoint;
            _player.Checkpoint = _checkpointPosition;
        }

        public override void Unexecute()
        {
            _player.Checkpoint = _previousPosition;
        }
    }
}
