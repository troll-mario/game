﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Entities;
using TrollMarioClient.Services;

namespace TrollMarioClient
{
    public class LoadCheckpoint : Command
    {
        Player _player;

        public LoadCheckpoint(Player player) : base(player)
        {
            _player = player;
            undoable = false;
        }

        public override void Execute()
        {
            _player.LoadCheckpoint();
        }

        public override void Unexecute()
        {
            throw new InvalidOperationException("Cannot return to dying position");
        }
    }
}
