﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Entities.Interfaces;
using TrollMarioClient.Utilities.Interpreter;

namespace TrollMarioClient.Utilities
{
    public class ConcreteIterator<T> : IEnumerator<T>
    {
        private int _index;
        private ConcreteContainer<T> _container;

        // For prioritising certain types
        private Type _priorityType;
        private int _secondaryIndex;
        private ConcreteContainer<T> _secondaryContainer;

        public T Current => throw new NotImplementedException();

        object IEnumerator.Current => throw new NotImplementedException();

        public ConcreteIterator(ConcreteContainer<T> container)
        {
            _index = 0;
            this._container = container;
            this._secondaryContainer = new ConcreteContainer<T>();
        }

        public bool HasNext()
        {
            if (_priorityType == null)
                return _index < _container.Count;
            else
            {
                if (_index < _container.Count)
                    return true;
                else
                    return _secondaryIndex < _secondaryContainer.Count;
            }
                
        }

        public T Next()
        {
            if(_priorityType == null)
            {
                if (HasNext())
                {
                    return (T)_container[_index++];
                }
                else
                {
                    throw new InvalidOperationException("Iterator does not have a next member");
                }
            }
            else
            {
                if (HasNext())
                {
                    for(int i = _index; i < _container.Count; i++)
                    {
                        if(_container[i].GetType() == _priorityType)
                        {
                            _index++;
                            return (T)_container[i];
                        }
                        else
                        {
                            _index++;
                            _secondaryContainer.Add((T)_container[i]);
                        }
                    }

                    return (T)_secondaryContainer[_secondaryIndex++];
                }
                else
                {
                    throw new InvalidOperationException("Iterator does not have a next member");
                }
            }
        }

        public T First()
        {
            if(_priorityType == null)
                return (T)_container[0];
            else
            {
                for(int i = 0; i < _container.Count; i++)
                {
                    if (_container[i].GetType() == _priorityType)
                        return (T)_container[i];
                }

                return (T)_container[0];

            }
        }

        public void Reset()
        {
            _index = 0;

            _secondaryIndex = 0;
            _secondaryContainer.Clear();
        }

        public bool MoveNext()
        {
            if (_priorityType == null && HasNext())
            {
                _index++;
                return true;
            }
            else if(HasNext())
            {
                if (_index < _container.Count)
                    _index++;
                else
                    _secondaryIndex++;

                return true;
            }
            else
            {
                return false;
            }
        }

        public void Dispose()
        {
            
        }

        public void SetPriorityType(Type t)
        {
            _priorityType = t;
        }
    }
}
