﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrollMarioClient.Entities.Interfaces
{
    public interface Iterator
    {
        public bool hasNext();
        public object next();
        public object first();
        public void reset();
    }
}
