﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Events;

namespace TrollMarioClient.Utilities.Memento
{
    public class PositionMemento
    {
        private Action<Vector2f> _actionToInvoke;
        private Vector2f _position;

        public PositionMemento(Action<Vector2f> action, Vector2f position)
        {
            _actionToInvoke = action;
            _position = position;
        }

        public void LoadFromMemento()
        {
            _actionToInvoke.Invoke(_position);
        }
    }
}
