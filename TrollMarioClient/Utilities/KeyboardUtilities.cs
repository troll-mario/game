﻿using SFML.Window;
using TrollMarioDomain.Enums;

namespace TrollMarioClient.Utilities
{
    public static class KeyboardUtilities
    {
        public static bool IsDirectionPressed(Direction direction)
        {
            return direction switch
            {
                Direction.Up => Keyboard.IsKeyPressed(Keyboard.Key.W) || Keyboard.IsKeyPressed(Keyboard.Key.Up),
                Direction.Right => Keyboard.IsKeyPressed(Keyboard.Key.D) || Keyboard.IsKeyPressed(Keyboard.Key.Right),
                Direction.Left => Keyboard.IsKeyPressed(Keyboard.Key.A) || Keyboard.IsKeyPressed(Keyboard.Key.Left),
                _ => false
            };
        }
    }
}