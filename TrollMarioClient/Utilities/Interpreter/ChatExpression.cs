﻿using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Services;
using TrollMarioClient.Utilities.Interpreter.ConsoleCommands;

namespace TrollMarioClient.Utilities.Interpreter
{
    public class ChatExpression : AbstractExpression
    {
        private const string _cheatCode = "SendMessage";

        public ChatExpression(ConsoleCommandChat command) : base(command)
        {

        }

        public override void Evaluate(InterpreterContext context)
        {
            EvaluateCommand(context, _cheatCode, _command);
        }

        //public override void Evaluate(InterpreterContext context)
        //{
        //    string command = context.Command;
        //    int startIndex = command.IndexOf(_cheatCode);
        //    while (startIndex != -1)
        //    {
        //        string isolatedCommand = Isolate(command, _cheatCode, 1);
        //        //Console.WriteLine("Isolated command: " + isolatedCommand);
        //        command = command.Remove(command.IndexOf(isolatedCommand), isolatedCommand.Length);
        //        context.Command = command;
        //        Console.WriteLine("Executing command: " + isolatedCommand);
        //        Console.WriteLine("New command: " + command);

        //        startIndex = command.IndexOf(_cheatCode, startIndex);
        //    }
        //}
    }
}
