﻿using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Services;

namespace TrollMarioClient.Utilities.Interpreter.ConsoleCommands
{
    public class ConsoleCommandChat : IConsoleCommand
    {
        ServerService _serverService;
        public ConsoleCommandChat(ServerService serverService)
        {
            _serverService = serverService;
        }

        public int ArgumentsRequired()
        {
            return 1;
        }

        public async void Execute(string[] args)
        {
            Console.WriteLine("Sending message: " + args[0]);
            await _serverService.ChatMessage(args[0]);
        }
    }
}
