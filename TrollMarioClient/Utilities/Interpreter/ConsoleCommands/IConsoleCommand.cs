﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrollMarioClient.Utilities.Interpreter.ConsoleCommands
{
    public interface IConsoleCommand
    {
        public int ArgumentsRequired();
        public void Execute(string[] args);
    }
}
