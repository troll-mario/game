﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrollMarioClient.Utilities.Interpreter.ConsoleCommands
{
    class ConsoleCommandTeleport : IConsoleCommand
    {
        public int ArgumentsRequired()
        {
            return 2;
        }

        public void Execute(string[] args)
        {
            Console.WriteLine("Teleporting player to coords: {0} {1}", args[0], args[1]);
        }
    }
}
