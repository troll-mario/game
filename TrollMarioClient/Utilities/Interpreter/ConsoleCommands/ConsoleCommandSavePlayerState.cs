﻿using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Services;
using TrollMarioClient.Utilities.Memento;

namespace TrollMarioClient.Utilities.Interpreter.ConsoleCommands
{
    class ConsoleCommandSavePlayerState : IConsoleCommand
    {
        private List<PositionMemento> _playerMementos;

        public ConsoleCommandSavePlayerState(List<PositionMemento> playerMementos)
        {
            _playerMementos = playerMementos;
        }

        public int ArgumentsRequired()
        {
            return 0;
        }

        public void Execute(string[] args)
        {
            _playerMementos.Add(StateServiceSingleton.GetInstance().SavePositionToMemento());
            Console.WriteLine("Player state saved!");
        }
    }
}
