﻿using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Entities;
using TrollMarioClient.Entities.Obstacles;
using TrollMarioClient.Services;
using TrollMarioClient.Utilities.Memento;

namespace TrollMarioClient.Utilities.Interpreter.ConsoleCommands
{
    class ConsoleCommandSetObstacleState : IConsoleCommand
    {
        private ObstacleDirector _obstacleDirector;

        public ConsoleCommandSetObstacleState(ObstacleDirector dir)
        {
            _obstacleDirector = dir;
        }

        public int ArgumentsRequired()
        {
            return 1;
        }

        public void Execute(string[] args)
        {
            _obstacleDirector.SetObstacleState(pickState(args[0]));
            Console.WriteLine("Obstacle states changed!");
        }

        private BaseObstacleState pickState(string state)
        {
            if (state == "Invisible")
            {
                return new InvisibleObstacleState();
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
