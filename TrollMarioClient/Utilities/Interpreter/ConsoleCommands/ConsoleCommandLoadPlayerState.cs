﻿using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Services;
using TrollMarioClient.Utilities.Memento;

namespace TrollMarioClient.Utilities.Interpreter.ConsoleCommands
{
    class ConsoleCommandLoadPlayerState : IConsoleCommand
    {
        private List<PositionMemento> _playerMementos;

        public ConsoleCommandLoadPlayerState(List<PositionMemento> playerMementos)
        {
            _playerMementos = playerMementos;
        }

        public int ArgumentsRequired()
        {
            return 0;
        }

        public void Execute(string[] args)
        {
            if(_playerMementos.Count > 0)
            {
                _playerMementos[_playerMementos.Count - 1].LoadFromMemento();
                _playerMementos.RemoveAt(_playerMementos.Count - 1);
                //StateServiceSingleton.GetInstance().LoadPositionFromMemento(_playerMementos[_playerMementos.Count - 1]);
                Console.WriteLine("Player state loaded!");
            }
        }
    }
}
