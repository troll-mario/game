﻿using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Utilities.Interpreter.ConsoleCommands;

namespace TrollMarioClient.Utilities.Interpreter
{
    public abstract class AbstractExpression
    {

        protected IConsoleCommand _command;

        public AbstractExpression(IConsoleCommand command)
        {
            _command = command;
        }

        public abstract void Evaluate(InterpreterContext context);

        public void EvaluateCommand(InterpreterContext context, string cheatCode, IConsoleCommand consoleCommand)
        {
            // Lots of hardcoding when fixing bugs, may need rewriting if issues are found
            string command = context.Command;
            int startIndex = command.IndexOf(cheatCode);
            while (startIndex != -1)
            {
                string isolatedCommand = IsolateCompleteCommand(command, cheatCode, consoleCommand.ArgumentsRequired());
                string[] arguments = IsolateArguments(isolatedCommand);
                if (arguments.Length == consoleCommand.ArgumentsRequired())
                {
                    consoleCommand.Execute(arguments);
                    command = command.Remove(command.IndexOf(isolatedCommand), isolatedCommand.Length);
                    context.Command = command;
                }
                else
                {
                    startIndex++;
                }
                
                startIndex = command.IndexOf(cheatCode, startIndex);
            }
        }

        

        public string IsolateCompleteCommand(string expression, string command, int argumentCount)
        {
            //string newExpression = "";
            string isolatedCommand = "";

            int startIndex = expression.IndexOf(command);
            int commandLength = command.Length;

            if (startIndex != 0 && expression[startIndex - 1] != ' ')
                return "";

            if (startIndex + commandLength < expression.Length && expression[startIndex + commandLength] != ' ')
                return "";

            string args = "";
            int argsCount = 0;
            // int checkedCount = 0;
            bool parenthesesOpen = false;
            for (int i = startIndex + commandLength; i < expression.Length; i++)
            {
                //checkedCount++;

                if (expression[i] == '"')
                {
                    if (parenthesesOpen)
                    {
                        parenthesesOpen = false;
                    }
                    else
                    {
                        parenthesesOpen = true;
                    }
                }

                if (expression[i] != ' ')
                {
                    args += expression[i];
                }
                else
                {
                    args += expression[i];
                    if (!parenthesesOpen)
                    {
                        argsCount++;
                        if (argsCount == argumentCount + 1)
                            break;
                    }
                }

            }
            isolatedCommand = expression.Substring(startIndex, commandLength) + args;
            //Console.WriteLine(args);
            //newExpression = expression.Remove(startIndex, commandLength + checkedCount);

            //return new Tuple<string, string>(newExpression, isolatedCommand);
            return isolatedCommand;

        }

        public string[] IsolateArguments(string isolatedCommand)
        {
            List<string> argList = new List<string>();
            string isolatedCommandArgs = isolatedCommand.Substring(isolatedCommand.IndexOf(' ') + 1);
            if (isolatedCommand == isolatedCommandArgs)
                return argList.ToArray();
            //Console.WriteLine("Isolated command is: " + isolatedCommand);
            string arg = "";
            bool parenthesesOpen = false;
            for (int i = 0; i < isolatedCommandArgs.Length; i++)
            {
                if (isolatedCommandArgs[i] == ' ' && !parenthesesOpen)
                {
                    if (arg != "")
                    {
                        argList.Add(arg);
                        arg = "";
                    }
                }
                else if (isolatedCommandArgs[i] == '"')
                {
                    if (parenthesesOpen)
                    {
                        parenthesesOpen = false;
                        argList.Add(arg);
                        arg = "";
                    }
                    else
                    {
                        parenthesesOpen = true;
                    }
                }
                else
                {
                    arg += isolatedCommandArgs[i];
                }
            }

            if (arg != "")
            {
                argList.Add(arg);
            }

            return argList.ToArray();
        }

        // Checks that cheat codes begin and end with a space or are at the start/end of the string
        protected bool IsIsolated(string expression, int startIndex, int count)
        {
            if (startIndex == 0 || (startIndex - 1 > 0 && expression[startIndex - 1] == ' '))
            {
                if (startIndex + count == expression.Length || expression[startIndex + count] == ' ')
                {
                    return true;
                }
            }
            return false;
        }

        protected List<int> GetCommandArguments(string expression, int startIndex, int argumentCount)
        {
            List<int> result = new List<int>();

            string argumentStr = "";
            int argumentsFound = 0;

            for (int i = startIndex; i < expression.Length; i++)
            {
                if (Char.IsDigit(expression[i]))
                {
                    argumentStr += expression[i];
                    if (i + 1 == expression.Length)
                    {
                        argumentsFound++;
                        break;
                    }
                    else if(expression[i+1] == ' ')
                    {
                        argumentsFound++;
                        argumentStr += ' ';
                        i++;
                    }

                }

                if (argumentsFound == argumentCount)
                    break;

            }

            result.Add(argumentStr.Length);

            string[] separateArgs = argumentStr.Trim().Split(' ');
            foreach(string s in separateArgs)
            {
                result.Add(Convert.ToInt32(s));
            }

            return result;
        }

    }
}
