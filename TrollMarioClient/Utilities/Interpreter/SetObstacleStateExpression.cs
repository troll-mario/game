﻿using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Utilities.Interpreter.ConsoleCommands;
using TrollMarioClient.Utilities.Memento;

namespace TrollMarioClient.Utilities.Interpreter
{
    class SetObstacleStateExpression : AbstractExpression
    {
        private const string _cheatCode = "SetObstacleState";

        public SetObstacleStateExpression(ConsoleCommandSetObstacleState command) : base(command)
        {

        }

        public override void Evaluate(InterpreterContext context)
        {
            EvaluateCommand(context, _cheatCode, _command);
        }
    }
}
