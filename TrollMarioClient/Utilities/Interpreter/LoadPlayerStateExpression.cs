﻿using System;
using System.Collections.Generic;
using System.Text;
using TrollMarioClient.Utilities.Interpreter.ConsoleCommands;
using TrollMarioClient.Utilities.Memento;

namespace TrollMarioClient.Utilities.Interpreter
{
    class LoadPlayerStateExpression : AbstractExpression
    {
        private const string _cheatCode = "LoadPlayerState";

        public LoadPlayerStateExpression(ConsoleCommandLoadPlayerState command) : base(command)
        {

        }

        public override void Evaluate(InterpreterContext context)
        {
            EvaluateCommand(context, _cheatCode, _command);
        }
    }
}
