﻿using SFML.Graphics;
using System;
using System.Threading.Tasks;
using TrollMarioClient.Entities.Obstacles;
using TrollMarioClient.Providers;
using TrollMarioClient.Services;

namespace TrollMarioClient
{
    internal class Game
    {
        private readonly DrawingService _drawingService;
        private readonly MainLoopService _mainLoopService;
        private readonly StateServiceSingleton _stateService;
        private readonly PlayerSpawnService _playerSpawnService;
        private readonly LevelSpawnService _levelSpawnService;
        private readonly ConsoleReaderService _consoleReadService;
        private readonly RenderWindow _window;
        private readonly ObstacleDirector _obstacleDirector;

        public Game
            (
                DrawingService drawingService,
                MainLoopService mainLoopService,
                PlayerSpawnService playerSpawnService,
                LevelSpawnService levelSpawnService,
                ConsoleReaderService consoleReadService,
                ObstacleDirector obstacleDirector

            )
        {
            _drawingService = drawingService;
            _mainLoopService = mainLoopService;
            _stateService = StateServiceSingleton.GetInstance();
            _playerSpawnService = playerSpawnService;
            _levelSpawnService = levelSpawnService;
            _consoleReadService = consoleReadService;
            _obstacleDirector = obstacleDirector;
            _window = GameWindowProvider.Window;

            Init();
        }

        public void Run()
        {
            _mainLoopService.Run();

            while (_window.IsOpen)
            {
                _window.DispatchEvents();

                _window.Clear(GameWindowProvider.Background);

                _stateService.Update();

                _drawingService.Draw();

                _window.Display();
            }
        }

        private void Init()
        {

            _levelSpawnService.SpawnLevel(_obstacleDirector);

            _consoleReadService.Start(_obstacleDirector);

            _playerSpawnService.SpawnPlayer();

            _window.Closed += (obj, args) => _window.Close();

            _mainLoopService.Init();
        }
    }
}
